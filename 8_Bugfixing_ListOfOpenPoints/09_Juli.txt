- Priorit�ten falsch konfiguriert
- Fehler bei Pointer-Casting in RTE (btSendData) --> �nderung zu Call-by-Value
- verbreitete Annahme, dass sich der Brick bei Verbindungsaufbau vor dem Klick auf "Run" aufh�ngt ist falsch! --> einfach warten
- in der Fahreinheit wird der hook f�r Bluetooth aufgerufen --> warum? 
- Bug mit z.b. "Stoppwaerts" behoben (Clear Display Line)
- �berpr�fung vor btSend aufruf ob sich der zu sendende Wert ver�ndert hat (Caching)
- Joystick Raw-Werte: links-rechts vertauscht
- Bluetooth Initialisierung mit Hooks realisiert, um RTE-Events zu vermeiden, bevor eine BT Verbindung zustande gekommen ist.