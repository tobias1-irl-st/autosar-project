#include "Rte.h" 

const char * clean = "                 ";

void clearLine(int line) {
	display_goto_xy(0, line);
	display_string(clean);
}

void printStringNumber(int line, const char *s, int number)
{	
	clearLine(line);
	display_goto_xy(0,line);
	display_string(s);
	display_int(number, 6);
	display_update();
} 

void printNumberString(int line, int number, const char *s) {
	clearLine(line);
	display_goto_xy(0,line);
	display_int(number, 6);
	display_string(s);
	display_update();
}

void printNumber(int line, int number)
{	
	clearLine(line);
	display_goto_xy(0,line);
	display_int(number, 4);
	display_update();
} 

void printString(int line, const char *s)
{		
	clearLine(line);
	display_goto_xy(0,line);
	display_string(s);
	display_update();
}

void printStatus(const char* name) {
	ecrobot_status_monitor(name);
}

/****************************************************
 * Display Runnables
 ****************************************************/
void DisplayDrawDirection() {
	uint8_t dir;    
	Rte_Read_Display_Display_Direction(&dir);
	if (dir == 0) { 
		printString(2, "Stopp");
	} else if (dir == 1){
		printString(2, "Vorwaerts");
	} else {        
		printString(2, "Rueckwaerts");
	}               
}                   
                    
void DisplayDrawSpeed() {
	int8_t speed;   
	Rte_Read_Display_Display_Velocity(&speed);
	printNumberString(1, speed, " meter/min");
}

void DisplayDrawDistance() {
	uint32_t usonicVal;
	Rte_Read_Display_Display_Distance(&usonicVal);
	printString(4, "Ultraschall:");
	printNumberString(5, usonicVal, " cm");
}

void DisplayDrawMotorStatus() {
	int8_t status;
	Rte_Read_Display_Display_MotorStatus(&status);
	if (status) {
		printString(0, "Motor: an");
	} else {
		printString(0, "Motor: aus");
	}
}

