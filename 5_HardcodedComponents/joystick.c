#include "Rte.h"

//#define I2C_ADDRESS 0x48
#define NORMALIZE_SWITCH(X) (X > 0) ? 0 : 1
#define Y_OFFSET 126
#define X_OFFSET 131
#define PORT_X 2
#define PORT_Y 1
#define PORT_SWITCH 0

int8_t limitRange(int32_t val) {
	if (val > 127) {
		val = 127;
	} else if (val < -127) {
		val = -127;
	}
	return (uint8_t) val;
}


void JoystickRunnable()
{
	uint8_t rawdata0, rawdata1, rawdata2;
	int8_t button;
	int8_t x, y;
	
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_SWITCH, &rawdata0, 1);
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_Y, &rawdata1, 1);
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_X, &rawdata2, 1);
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_SWITCH, &rawdata0, 1);
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_Y, &rawdata1, 1);
	ecrobot_read_i2c(Rte_IO_Joystick, Rte_IO_Joystick_Address, PORT_X, &rawdata2, 1);
	
	button = NORMALIZE_SWITCH(rawdata0);
	x = limitRange(rawdata2 - X_OFFSET) * -1;
	y = limitRange(rawdata1 - Y_OFFSET);
	
	
	Rte_Write_Joystick_Joystick_DirectionY(y);
	Rte_Write_Joystick_Joystick_DirectionX(x);
	Rte_Write_Joystick_Joystick_ButtonPressed(button);
}

