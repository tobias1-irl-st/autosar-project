#include "Rte.h"

static uint8_t WHEEL_DIAMETER = 40;
#define PI (3.1415)
#define J_TOLERANCE 4


void LogicProcessJoystickInfo()
{
	static uint8_t BACKWARD_AUDIO_FREQ = 450;
	static uint8_t BACKWARD_PEEP_FREQ = 100;
    int8_t joystickX, joystickY, buttonPressed;
    static uint8_t old_peep_status = 0;
	static uint8_t old_button_status = 0;
	static uint8_t isDriving = 0;
	static uint8_t peepNotEnable = 0;
	static uint8_t peepEnable = 1;
	static uint8_t motorOn = 1;
	static uint8_t motorOff = 0;
	
    Rte_Read_Logic_Logic_DirectionX(&joystickX);
    Rte_Read_Logic_Logic_DirectionY(&joystickY);
	Rte_Read_Logic_Logic_ButtonPressed(&buttonPressed);
	
	if (old_button_status == 0 && buttonPressed == 1)
	{
		// button wurde gedrueckt
		if (isDriving) {
			isDriving = 0;
			Rte_Write_Logic_Logic_MotorStatus(motorOff);
		} else {
			isDriving = 1;
			Rte_Write_Logic_Logic_MotorStatus(motorOn);
		}
	}
	
	old_button_status = buttonPressed;
	
	if(!isDriving){
		joystickX = 0;
		joystickY = 0;
	}

    Rte_Write_Logic_Logic_DirectionX(joystickX);
    Rte_Write_Logic_Logic_DirectionY(joystickY);


    // Peep
    if (joystickY < -J_TOLERANCE)
    {
        Rte_Write_Logic_Logic_AudioFreq(BACKWARD_AUDIO_FREQ);
        Rte_Write_Logic_Logic_PeepFreq(BACKWARD_PEEP_FREQ);
        Rte_Write_Logic_Logic_PeepEnable(peepEnable);
        old_peep_status = 1;
    }
    else if (joystickY >= -J_TOLERANCE)
    {
        Rte_Write_Logic_Logic_PeepEnable(peepNotEnable);
        old_peep_status = 0;
    }
}

void LogicProcessDriveInfo()
{
    int32_t motorRotationLeft, motorRotationRight;
    int8_t meterPerMinuteLeft, meterPerMinuteRight, meterPerMinute;
	int8_t forward = 1;
	int8_t backward = 2;
	int8_t stop = 0;
	int8_t jY = 0;

    Rte_Read_Logic_Logic_RPM_left(&motorRotationLeft);
    Rte_Read_Logic_Logic_RPM_right(&motorRotationRight);
	Rte_Read_Logic_Logic_DirectionY(&jY);

	
    meterPerMinuteLeft = motorRotationLeft * WHEEL_DIAMETER * PI / 1000;
    meterPerMinuteRight = motorRotationRight * WHEEL_DIAMETER * PI / 1000;
    meterPerMinute = (meterPerMinuteLeft + meterPerMinuteRight) / 2;
    Rte_Write_Logic_Logic_Velocity(meterPerMinute);

    if (jY > J_TOLERANCE)
        Rte_Write_Logic_Logic_Direction(forward); // vorwärts
    else if (jY < -J_TOLERANCE)
        Rte_Write_Logic_Logic_Direction(backward); // Rückwärts
    else
        Rte_Write_Logic_Logic_Direction(stop); // Stop
}
