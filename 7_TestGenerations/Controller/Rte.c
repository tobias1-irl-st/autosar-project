/*********************************************************************
 * Rte source file
 * Time generated: 2018/07/09 16:58:07
 *********************************************************************/
#include "Rte.h"

/*********************************************************************
 * Rte global variables
 *********************************************************************/
int8_t Rte_Joystick_Joystick_DirectionX;
int8_t Rte_Joystick_Joystick_DirectionY;
int8_t Rte_Joystick_Joystick_ButtonPressed;
int8_t Rte_Logic_Logic_DirectionX;
int8_t Rte_Logic_Logic_DirectionY;
int32_t Rte_Logic_Logic_Velocity;
uint32_t Rte_Logic_Logic_AudioFreq;
uint8_t Rte_Logic_Logic_PeepEnable;
uint8_t Rte_Logic_Logic_PeepFreq;
uint8_t Rte_Logic_Logic_Direction;
int32_t Rte_DriveUnit_DriveUnit_RPM_Left;
int32_t Rte_DriveUnit_DriveUnit_RPM_right;
uint32_t Rte_DriveUnit_DriveUnit_Sonar;
uint8_t Rte_Logic_Logic_MotorStatus;

/*********************************************************************
 * Rte_Write functions
 *********************************************************************/
