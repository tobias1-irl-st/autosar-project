#include "tasks.h"

void user_1ms_isr_type2(void){
  StatusType ercd;
  ercd = SignalCounter(Ctr_1_ms);
  if( ercd != E_OK ){
    ShutdownOS( ercd );
  }
}

extern void DisplayDrawSpeed();
extern void DisplayDrawDistance();
extern void JoystickRunnable();
extern void Bluetooth();
extern void LogicProcessJoystickInfo();
extern void DisplayDrawDirection();
extern void LogicProcessDriveInfo();
extern void DisplayDrawMotorStatus();

TASK(PollJoystick)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_TM_Cyclic_250);
    GetEvent(PollJoystick, &event);
    ClearEvent(Rte_Ev_TM_Cyclic_250);

    if (event & (Rte_Ev_TM_Cyclic_250)) {
      JoystickRunnable();
    }

  }
  TerminateTask();
}

TASK(MainTask)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_DR_Display_Display_Direction | Rte_Ev_DR_Display_Display_Distance | Rte_Ev_DR_Display_Display_Velocity | Rte_Ev_DR_Logic_Logic_RPM_left | Rte_Ev_DR_Logic_Logic_RPM_right | Rte_Ev_DR_Logic_Logic_DirectionX | Rte_Ev_DR_Logic_Logic_DirectionY | Rte_Ev_DR_Logic_Logic_ButtonPressed | Rte_Ev_DR_Display_Display_MotorStatus);
    GetEvent(MainTask, &event);
    ClearEvent(Rte_Ev_DR_Display_Display_Direction | Rte_Ev_DR_Display_Display_Distance | Rte_Ev_DR_Display_Display_Velocity | Rte_Ev_DR_Logic_Logic_RPM_left | Rte_Ev_DR_Logic_Logic_RPM_right | Rte_Ev_DR_Logic_Logic_DirectionX | Rte_Ev_DR_Logic_Logic_DirectionY | Rte_Ev_DR_Logic_Logic_ButtonPressed | Rte_Ev_DR_Display_Display_MotorStatus);

    if (event & (Rte_Ev_DR_Display_Display_Direction)) {
      DisplayDrawDirection();
    }

    if (event & (Rte_Ev_DR_Display_Display_Distance)) {
      DisplayDrawDistance();
    }

    if (event & (Rte_Ev_DR_Display_Display_Velocity)) {
      DisplayDrawSpeed();
    }

    if (event & (Rte_Ev_DR_Logic_Logic_RPM_left | Rte_Ev_DR_Logic_Logic_RPM_right)) {
      LogicProcessDriveInfo();
    }

    if (event & (Rte_Ev_DR_Logic_Logic_DirectionX | Rte_Ev_DR_Logic_Logic_DirectionY | Rte_Ev_DR_Logic_Logic_ButtonPressed)) {
      LogicProcessJoystickInfo();
    }

    if (event & (Rte_Ev_DR_Display_Display_MotorStatus)) {
      DisplayDrawMotorStatus();
    }

  }
  TerminateTask();
}

TASK(PollBt)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_TM_Cyclic_1);
    GetEvent(PollBt, &event);
    ClearEvent(Rte_Ev_TM_Cyclic_1);

    if (event & (Rte_Ev_TM_Cyclic_1)) {
      Bluetooth();
    }

  }
  TerminateTask();
}

