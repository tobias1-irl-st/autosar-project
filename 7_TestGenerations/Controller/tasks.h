#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"


DeclareTask(MainTask);
DeclareTask(PollBt);
DeclareTask(PollJoystick);
DeclareCounter(Ctr_1_ms);
DeclareAlarm(al_250_ms_Rte_Ev_TM_Cyclic_250_PollJoystick);
DeclareAlarm(al_1_ms_Rte_Ev_TM_Cyclic_1_PollBt);
DeclareEvent(Rte_Ev_DR_Logic_Logic_RPM_left);
DeclareEvent(Rte_Ev_DR_Logic_Logic_ButtonPressed);
DeclareEvent(Rte_Ev_TM_Cyclic_1);
DeclareEvent(Rte_Ev_DR_Display_Display_Distance);
DeclareEvent(Rte_Ev_DR_Logic_Logic_DirectionX);
DeclareEvent(Rte_Ev_DR_Display_Display_MotorStatus);
DeclareEvent(Rte_Ev_DR_Logic_Logic_DirectionY);
DeclareEvent(Rte_Ev_DR_Display_Display_Velocity);
DeclareEvent(Rte_Ev_DR_Display_Display_Direction);
DeclareEvent(Rte_Ev_TM_Cyclic_250);
DeclareEvent(Rte_Ev_DR_Logic_Logic_RPM_right);

