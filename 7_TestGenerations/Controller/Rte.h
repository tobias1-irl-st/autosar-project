/*********************************************************************
 * Rte header file
 * Time generated: 2018/07/09 16:58:07
 *********************************************************************/

#ifndef RTE_h
#define RTE_h

#include "ecrobot_interface.h"
#include <stdint.h>
#include "tasks.h"

extern void btSendData(uint16_t, uint32_t, uint8_t);

/*********************************************************************
 * Rte global variables
 *********************************************************************/
extern int8_t Rte_Joystick_Joystick_DirectionX;
extern int8_t Rte_Joystick_Joystick_DirectionY;
extern int8_t Rte_Joystick_Joystick_ButtonPressed;
extern int8_t Rte_Logic_Logic_DirectionX;
extern int8_t Rte_Logic_Logic_DirectionY;
extern int32_t Rte_Logic_Logic_Velocity;
extern uint32_t Rte_Logic_Logic_AudioFreq;
extern uint8_t Rte_Logic_Logic_PeepEnable;
extern uint8_t Rte_Logic_Logic_PeepFreq;
extern uint8_t Rte_Logic_Logic_Direction;
extern int32_t Rte_DriveUnit_DriveUnit_RPM_Left;
extern int32_t Rte_DriveUnit_DriveUnit_RPM_right;
extern uint32_t Rte_DriveUnit_DriveUnit_Sonar;
extern uint8_t Rte_Logic_Logic_MotorStatus;

/*********************************************************************
 * Rte write function declaration
 *********************************************************************/

/*********************************************************************
 * Rte_Read Receivers
 *********************************************************************/
#define Rte_Read_Logic_Logic_DirectionX(data) (*(data) = Rte_Joystick_Joystick_DirectionX)
#define Rte_Read_Logic_Logic_DirectionY(data) (*(data) = Rte_Joystick_Joystick_DirectionY)
#define Rte_Read_Logic_Logic_ButtonPressed(data) (*(data) = Rte_Joystick_Joystick_ButtonPressed)
#define Rte_Read_DriveUnit_DriveUnit_DirectionX(data) (*(data) = Rte_Logic_Logic_DirectionX)
#define Rte_Read_DriveUnit_DriveUnit_DirectionY(data) (*(data) = Rte_Logic_Logic_DirectionY)
#define Rte_Read_Display_Display_Velocity(data) (*(data) = Rte_Logic_Logic_Velocity)
#define Rte_Read_Audio_Audio_AudioFreq(data) (*(data) = Rte_Logic_Logic_AudioFreq)
#define Rte_Read_Audio_Audio_PeepEnable(data) (*(data) = Rte_Logic_Logic_PeepEnable)
#define Rte_Read_Audio_Audio_PeepFreq(data) (*(data) = Rte_Logic_Logic_PeepFreq)
#define Rte_Read_Display_Display_Direction(data) (*(data) = Rte_Logic_Logic_Direction)
#define Rte_Read_Logic_Logic_RPM_left(data) (*(data) = Rte_DriveUnit_DriveUnit_RPM_Left)
#define Rte_Read_Logic_Logic_RPM_right(data) (*(data) = Rte_DriveUnit_DriveUnit_RPM_right)
#define Rte_Read_Display_Display_Distance(data) (*(data) = Rte_DriveUnit_DriveUnit_Sonar)
#define Rte_Read_DriveUnit_Drive_Unit_Sonar_Value(data) (*(data) = Rte_DriveUnit_DriveUnit_Sonar)
#define Rte_Read_Display_Display_MotorStatus(data) (*(data) = Rte_Logic_Logic_MotorStatus)

/*********************************************************************
 * Rte_Write Senders
 *********************************************************************/
#define Rte_Write_Joystick_Joystick_DirectionX(data) (Rte_Joystick_Joystick_DirectionX = (data), SetEvent(MainTask, Rte_Ev_DR_Logic_Logic_DirectionX))
#define Rte_Write_Joystick_Joystick_DirectionY(data) (Rte_Joystick_Joystick_DirectionY = (data), SetEvent(MainTask, Rte_Ev_DR_Logic_Logic_DirectionY))
#define Rte_Write_Joystick_Joystick_ButtonPressed(data) (Rte_Joystick_Joystick_ButtonPressed = (data), SetEvent(MainTask, Rte_Ev_DR_Logic_Logic_ButtonPressed))
#define Rte_Write_Logic_Logic_DirectionX(data) (btSendData(300, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_Logic_Logic_DirectionY(data) (btSendData(301, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_Logic_Logic_Velocity(data) (Rte_Logic_Logic_Velocity = (data), SetEvent(MainTask, Rte_Ev_DR_Display_Display_Velocity))
#define Rte_Write_Logic_Logic_AudioFreq(data) (btSendData(310, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_Logic_Logic_PeepEnable(data) (btSendData(311, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_Logic_Logic_PeepFreq(data) (btSendData(312, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_Logic_Logic_Direction(data) (Rte_Logic_Logic_Direction = (data), SetEvent(MainTask, Rte_Ev_DR_Display_Display_Direction))
#define Rte_Write_BtRcvData_DriveUnit_DriveUnit_RPM_Left(data) (Rte_DriveUnit_DriveUnit_RPM_Left = (data), SetEvent(MainTask, Rte_Ev_DR_Logic_Logic_RPM_left))
#define Rte_Write_BtRcvData_DriveUnit_DriveUnit_RPM_right(data) (Rte_DriveUnit_DriveUnit_RPM_right = (data), SetEvent(MainTask, Rte_Ev_DR_Logic_Logic_RPM_right))
#define Rte_Write_BtRcvData_DriveUnit_DriveUnit_Sonar(data) (Rte_DriveUnit_DriveUnit_Sonar = (data), SetEvent(MainTask, Rte_Ev_DR_Display_Display_Distance))
#define Rte_Write_Logic_Logic_MotorStatus(data) (Rte_Logic_Logic_MotorStatus = (data), SetEvent(MainTask, Rte_Ev_DR_Display_Display_MotorStatus))

/*********************************************************************
 * Rte hardware in/out
 *********************************************************************/
#define Rte_IO_Joystick (NXT_PORT_S1)
#define Rte_IO_Joystick_Address (72)
#define Rte_IO_Engine_Left (NXT_PORT_A)
#define Rte_IO_Engine_Right (NXT_PORT_B)
#define Rte_IO_UltraSonic (NXT_PORT_S1)
#endif
