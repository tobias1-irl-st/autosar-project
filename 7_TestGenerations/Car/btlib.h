#include "Rte.h"


void beginConnectionSlave() {
    while (ecrobot_get_bt_status() != BT_STREAM) {
      ecrobot_init_bt_slave("1234");
    }
}

void beginConnectionMaster(U8 slaveAddr[7])

{
	while(ecrobot_get_bt_status() != BT_STREAM)
	ecrobot_init_bt_master(slaveAddr, "1234"); //Maximal 16 Zeichen!
}


typedef struct bt_packet {
  uint16_t id;
  uint8_t length;
  uint32_t payload;
} BtPacket;
void btSendData(uint16_t id, uint32_t payload, uint8_t length) {
	static int8_t old_200 = 0;
	static int8_t old_201 = 0;
	static int8_t old_202 = 0;
	static int8_t old_300 = 0;
	static int8_t old_301 = 0;
	static int32_t old_306 = 0;
	static uint32_t old_310 = 0;
	static uint8_t old_311 = 0;
	static uint8_t old_312 = 0;
	static uint8_t old_313 = 0;
	static int32_t old_400 = 0;
	static int32_t old_402 = 0;
	static uint32_t old_900 = 0;
	static uint8_t old_314 = 0;
  static BtPacket p;
  p.id = id;
  p.length = length;
  p.payload = payload;
	if((id == 200) && (((int8_t) payload) != old_200)) {
		 old_200 = payload;
	} else 	if((id == 201) && (((int8_t) payload) != old_201)) {
		 old_201 = payload;
	} else 	if((id == 202) && (((int8_t) payload) != old_202)) {
		 old_202 = payload;
	} else 	if((id == 300) && (((int8_t) payload) != old_300)) {
		 old_300 = payload;
	} else 	if((id == 301) && (((int8_t) payload) != old_301)) {
		 old_301 = payload;
	} else 	if((id == 306) && (((int32_t) payload) != old_306)) {
		 old_306 = payload;
	} else 	if((id == 310) && (((uint32_t) payload) != old_310)) {
		 old_310 = payload;
	} else 	if((id == 311) && (((uint8_t) payload) != old_311)) {
		 old_311 = payload;
	} else 	if((id == 312) && (((uint8_t) payload) != old_312)) {
		 old_312 = payload;
	} else 	if((id == 313) && (((uint8_t) payload) != old_313)) {
		 old_313 = payload;
	} else 	if((id == 400) && (((int32_t) payload) != old_400)) {
		 old_400 = payload;
	} else 	if((id == 402) && (((int32_t) payload) != old_402)) {
		 old_402 = payload;
	} else 	if((id == 900) && (((uint32_t) payload) != old_900)) {
		 old_900 = payload;
	} else 	if((id == 314) && (((uint8_t) payload) != old_314)) {
		 old_314 = payload;
	} else { return; }
		systick_wait_ms(10);
		ecrobot_send_bt_packet((void *) &p, sizeof(struct bt_packet));
}

BtPacket read_bt_packet() {
  static BtPacket p;
	 ecrobot_read_bt_packet((void *) &p, sizeof(struct bt_packet));
  return p;
}

void btRecvData() {
  BtPacket p;
  p.length = 0;
  p = read_bt_packet();
  if(p.length > 0) {
    switch (p.id) {
    case 200:
      break;
    case 201:
      break;
    case 202:
      break;
    case 300:
		Rte_Write_BtRcvData_Logic_Logic_DirectionX((int8_t) p.payload);
      break;
    case 301:
		Rte_Write_BtRcvData_Logic_Logic_DirectionY((int8_t) p.payload);
      break;
    case 306:
      break;
    case 310:
		Rte_Write_BtRcvData_Logic_Logic_AudioFreq((uint32_t) p.payload);
      break;
    case 311:
		Rte_Write_BtRcvData_Logic_Logic_PeepEnable((uint8_t) p.payload);
      break;
    case 312:
		Rte_Write_BtRcvData_Logic_Logic_PeepFreq((uint8_t) p.payload);
      break;
    case 313:
      break;
    case 400:
      break;
    case 402:
      break;
    case 900:
      break;
    case 314:
      break;
    }
  }
}
