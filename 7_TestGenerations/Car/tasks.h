#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"


DeclareTask(MainTask);
DeclareTask(PollSonar);
DeclareTask(PollBt);
DeclareCounter(Ctr_1_ms);
DeclareAlarm(al_100_ms_Rte_Ev_TM_Cyclic_100_PollSonar);
DeclareAlarm(al_1_ms_Rte_Ev_TM_Cyclic_1_PollBt);
DeclareEvent(Rte_Ev_TM_Cyclic_100);
DeclareEvent(Rte_Ev_DR_DriveUnit_Drive_Unit_Sonar_Value);
DeclareEvent(Rte_Ev_TM_Cyclic_1);
DeclareEvent(Rte_Ev_DR_Audio_Audio_PeepFreq);
DeclareEvent(Rte_Ev_DR_Audio_Audio_AudioFreq);
DeclareEvent(Rte_Ev_DR_Audio_Audio_PeepEnable);
DeclareEvent(Rte_Ev_DR_DriveUnit_DriveUnit_DirectionX);
DeclareEvent(Rte_Ev_DR_DriveUnit_DriveUnit_DirectionY);

