/*********************************************************************
 * Rte header file
 * Time generated: 2018/07/09 16:58:07
 *********************************************************************/

#ifndef RTE_h
#define RTE_h

#include "ecrobot_interface.h"
#include <stdint.h>
#include "tasks.h"

extern void btSendData(uint16_t, uint32_t, uint8_t);

/*********************************************************************
 * Rte global variables
 *********************************************************************/
extern int8_t Rte_Joystick_Joystick_DirectionX;
extern int8_t Rte_Joystick_Joystick_DirectionY;
extern int8_t Rte_Joystick_Joystick_ButtonPressed;
extern int8_t Rte_Logic_Logic_DirectionX;
extern int8_t Rte_Logic_Logic_DirectionY;
extern int32_t Rte_Logic_Logic_Velocity;
extern uint32_t Rte_Logic_Logic_AudioFreq;
extern uint8_t Rte_Logic_Logic_PeepEnable;
extern uint8_t Rte_Logic_Logic_PeepFreq;
extern uint8_t Rte_Logic_Logic_Direction;
extern int32_t Rte_DriveUnit_DriveUnit_RPM_Left;
extern int32_t Rte_DriveUnit_DriveUnit_RPM_right;
extern uint32_t Rte_DriveUnit_DriveUnit_Sonar;
extern uint8_t Rte_Logic_Logic_MotorStatus;

/*********************************************************************
 * Rte write function declaration
 *********************************************************************/
extern void Rte_Write_DriveUnit_DriveUnit_Sonar( uint32_t data);

/*********************************************************************
 * Rte_Read Receivers
 *********************************************************************/
#define Rte_Read_Logic_Logic_DirectionX(data) (*(data) = Rte_Joystick_Joystick_DirectionX)
#define Rte_Read_Logic_Logic_DirectionY(data) (*(data) = Rte_Joystick_Joystick_DirectionY)
#define Rte_Read_Logic_Logic_ButtonPressed(data) (*(data) = Rte_Joystick_Joystick_ButtonPressed)
#define Rte_Read_DriveUnit_DriveUnit_DirectionX(data) (*(data) = Rte_Logic_Logic_DirectionX)
#define Rte_Read_DriveUnit_DriveUnit_DirectionY(data) (*(data) = Rte_Logic_Logic_DirectionY)
#define Rte_Read_Display_Display_Velocity(data) (*(data) = Rte_Logic_Logic_Velocity)
#define Rte_Read_Audio_Audio_AudioFreq(data) (*(data) = Rte_Logic_Logic_AudioFreq)
#define Rte_Read_Audio_Audio_PeepEnable(data) (*(data) = Rte_Logic_Logic_PeepEnable)
#define Rte_Read_Audio_Audio_PeepFreq(data) (*(data) = Rte_Logic_Logic_PeepFreq)
#define Rte_Read_Display_Display_Direction(data) (*(data) = Rte_Logic_Logic_Direction)
#define Rte_Read_Logic_Logic_RPM_left(data) (*(data) = Rte_DriveUnit_DriveUnit_RPM_Left)
#define Rte_Read_Logic_Logic_RPM_right(data) (*(data) = Rte_DriveUnit_DriveUnit_RPM_right)
#define Rte_Read_Display_Display_Distance(data) (*(data) = Rte_DriveUnit_DriveUnit_Sonar)
#define Rte_Read_DriveUnit_Drive_Unit_Sonar_Value(data) (*(data) = Rte_DriveUnit_DriveUnit_Sonar)
#define Rte_Read_Display_Display_MotorStatus(data) (*(data) = Rte_Logic_Logic_MotorStatus)

/*********************************************************************
 * Rte_Write Senders
 *********************************************************************/
#define Rte_Write_BtRcvData_Logic_Logic_DirectionX(data) (Rte_Logic_Logic_DirectionX = (data), SetEvent(PollSonar, Rte_Ev_DR_DriveUnit_DriveUnit_DirectionX))
#define Rte_Write_BtRcvData_Logic_Logic_DirectionY(data) (Rte_Logic_Logic_DirectionY = (data), SetEvent(PollSonar, Rte_Ev_DR_DriveUnit_DriveUnit_DirectionY))
#define Rte_Write_BtRcvData_Logic_Logic_AudioFreq(data) (Rte_Logic_Logic_AudioFreq = (data), SetEvent(MainTask, Rte_Ev_DR_Audio_Audio_AudioFreq))
#define Rte_Write_BtRcvData_Logic_Logic_PeepEnable(data) (Rte_Logic_Logic_PeepEnable = (data), SetEvent(MainTask, Rte_Ev_DR_Audio_Audio_PeepEnable))
#define Rte_Write_BtRcvData_Logic_Logic_PeepFreq(data) (Rte_Logic_Logic_PeepFreq = (data), SetEvent(MainTask, Rte_Ev_DR_Audio_Audio_PeepFreq))
#define Rte_Write_DriveUnit_DriveUnit_RPM_Left(data) (btSendData(400, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_DriveUnit_DriveUnit_RPM_right(data) (btSendData(402, (uint32_t)(data), sizeof(uint32_t)))
#define Rte_Write_DriveUnit_DriveUnit_Sonar Rte_Write_DriveUnit_DriveUnit_Sonar

/*********************************************************************
 * Rte hardware in/out
 *********************************************************************/
#define Rte_IO_Joystick (NXT_PORT_S1)
#define Rte_IO_Joystick_Address (72)
#define Rte_IO_Engine_Left (NXT_PORT_A)
#define Rte_IO_Engine_Right (NXT_PORT_B)
#define Rte_IO_UltraSonic (NXT_PORT_S1)
#endif
