#include "tasks.h"

void user_1ms_isr_type2(void){
  StatusType ercd;
  ercd = SignalCounter(Ctr_1_ms);
  if( ercd != E_OK ){
    ShutdownOS( ercd );
  }
}

extern void DriveRunnable();
extern void Bluetooth();
extern void SonarRunnable();
extern void AudioRunnable();

TASK(MainTask)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_DR_DriveUnit_Drive_Unit_Sonar_Value | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionX | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionY | Rte_Ev_DR_Audio_Audio_AudioFreq | Rte_Ev_DR_Audio_Audio_PeepEnable | Rte_Ev_DR_Audio_Audio_PeepFreq);
    GetEvent(MainTask, &event);
    ClearEvent(Rte_Ev_DR_DriveUnit_Drive_Unit_Sonar_Value | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionX | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionY | Rte_Ev_DR_Audio_Audio_AudioFreq | Rte_Ev_DR_Audio_Audio_PeepEnable | Rte_Ev_DR_Audio_Audio_PeepFreq);

    if (event & (Rte_Ev_DR_DriveUnit_Drive_Unit_Sonar_Value | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionX | Rte_Ev_DR_DriveUnit_DriveUnit_DirectionY)) {
      DriveRunnable();
    }

    if (event & (Rte_Ev_DR_Audio_Audio_AudioFreq | Rte_Ev_DR_Audio_Audio_PeepEnable | Rte_Ev_DR_Audio_Audio_PeepFreq)) {
      AudioRunnable();
    }

  }
  TerminateTask();
}

TASK(PollSonar)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_TM_Cyclic_100);
    GetEvent(PollSonar, &event);
    ClearEvent(Rte_Ev_TM_Cyclic_100);

    if (event & (Rte_Ev_TM_Cyclic_100)) {
      SonarRunnable();
    }

  }
  TerminateTask();
}

TASK(PollBt)
{
  EventMaskType event;
  while (1) {
    WaitEvent(Rte_Ev_TM_Cyclic_1);
    GetEvent(PollBt, &event);
    ClearEvent(Rte_Ev_TM_Cyclic_1);

    if (event & (Rte_Ev_TM_Cyclic_1)) {
      Bluetooth();
    }

  }
  TerminateTask();
}

