#include "Rte.h"

static int32_t last_rev_left = 0;
static int32_t last_rev_right = 0;

void ecrobot_device_terminate(void)	//runs automatically at shutdown
{
	/* Terminate ECRobot used devices */
	ecrobot_term_sonar_sensor(Rte_IO_UltraSonic);
	nxt_motor_set_speed(Rte_IO_Engine_Right, 0, 1);
	nxt_motor_set_speed(Rte_IO_Engine_Left, 0, 1);
	nxt_motor_set_count(Rte_IO_Engine_Right, 0);
	nxt_motor_set_count(Rte_IO_Engine_Left, 0);
}

void SonarRunnable()	//read new sensor value to port
{
	static int init = 0;
	if(!init) {
		ecrobot_init_sonar_sensor(Rte_IO_UltraSonic);
		init++;
	}
	int32_t i = ecrobot_get_sonar_sensor(Rte_IO_UltraSonic);
	Rte_Write_DriveUnit_DriveUnit_Sonar(i);
}

void DriveRunnable()
{
	int32_t sonar_value;
	Rte_Read_DriveUnit_Drive_Unit_Sonar_Value(&sonar_value);
	S8 joy_x = 0;
	S8 joy_y = 0;
	Rte_Read_DriveUnit_DriveUnit_DirectionX(&joy_x);
	Rte_Read_DriveUnit_DriveUnit_DirectionY(&joy_y);
	// jeder wert ein S8 Port
	joy_y /= 1.28;
	joy_x /= 1.28;
	int speed_r = joy_y;
	int speed_l = joy_y;
	if(joy_x > 0)
	{
		speed_l += joy_x;
	}
	else if (joy_x < 0)
	{
		speed_r += (-1* joy_x);
	}
	//TODO PID with motor counts
	if(sonar_value < 30)
	{
		if(speed_r >= 0)
			speed_r = 0;
		if(speed_l >= 0)
			speed_l = 0;
	}
	//Parameters: NXT_Port, speed_percent, brake (0:=float 1:=brake)
	nxt_motor_set_speed(Rte_IO_Engine_Left, speed_l, 1);
	nxt_motor_set_speed(Rte_IO_Engine_Right, speed_r, 1);
	
	int32_t rev_left = ecrobot_get_motor_rev(Rte_IO_Engine_Left);
	int32_t rev_right = ecrobot_get_motor_rev(Rte_IO_Engine_Right);
	//Timed every 50 ms (= 20x per sec, 1200x per min)
	//TODO verfeinern!
	int32_t rpm_left = (rev_left - last_rev_left) * 1000/ (360 / 20) ;
	int32_t rpm_right = (rev_right - last_rev_right) * 1000/ (360 / 20 ) ;
	rpm_left = (rpm_left / 100) * 100;
	rpm_right = (rpm_right / 100) * 100;
	Rte_Write_DriveUnit_DriveUnit_RPM_Left(rpm_left);
	Rte_Write_DriveUnit_DriveUnit_RPM_right(rpm_right);
	last_rev_left = rev_left;
	last_rev_right = rev_right;
	
}
