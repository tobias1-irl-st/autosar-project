#include "Rte.h"

void AudioRunnable() {
	uint8_t tone_frequence;
	uint8_t peep_frequence;
	uint8_t enable;
	
	Rte_Read_Audio_Audio_AudioFreq(&tone_frequence);
	Rte_Read_Audio_Audio_PeepFreq(&peep_frequence);
	Rte_Read_Audio_Audio_PeepEnable(&enable);
	
	if(enable) {
		ecrobot_sound_tone(tone_frequence, peep_frequence, 100);
	}
}
