#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"
#include "printlib.h"
#include "btlib.h" 

DeclareTask(Disp);
DeclareTask(Main);
DeclareTask(RecvBt);
DeclareCounter(Ctr_1ms);
DeclareAlarm(al_poll_bt);
DeclareEvent(evt_disp_speed);

#include "functions.h"

TASK(RecvBt)
{	
	BtPacket p = read_bt_packet();
	if(p.type == SPEED_DATA) {
		set_speed(p.data);
		WriteTriggerPort_speed();
	}
	TerminateTask();
}

TASK(Disp)
{
	while(1) {
		WaitTriggerPort_speed();
		int dir = ReadPort_speed();
		if (dir < 0) 
			printStringNumber("rueckwaerts: ", -dir);
		else
			printStringNumber("vorwaerts: ", dir);
		
	}
	TerminateTask();
}


TASK(Main)
{
	printString("Wait...");
	while(ecrobot_get_bt_status()!=BT_STREAM)
		ecrobot_init_bt_slave("AUTOSAR_VL");
	
	printString("Connected");
	ActivateTask(Disp);
	TerminateTask();
}
