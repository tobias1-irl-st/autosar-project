static int direction = 0;

void user_1ms_isr_type2(void){ 
	StatusType ercd;
	ercd = SignalCounter(Ctr_1ms);
	if( ercd != E_OK ){
		ShutdownOS( ercd );
	}
} 

int ReadPort_speed()
{
	return direction;
}

void set_speed(int number) {
	direction = number;
}

void WaitTriggerPort_speed()
{
	WaitEvent(evt_disp_speed);
	ClearEvent(evt_disp_speed);
}

void WriteTriggerPort_speed() {
	SetEvent(Disp, evt_disp_speed);
}
