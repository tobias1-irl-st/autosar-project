#ifndef BTLIB_H
#define BTLIB_H

typedef enum {EVENT = 0, SPEED_DATA, INVALID/*may not be transmitted via bt*/} BtPacketDataType;

typedef struct bt_packet {
	unsigned type;
	unsigned data;
} BtPacket;

void send_bt_packet(BtPacketDataType t, unsigned data) {
	static BtPacket p;
	if(t == INVALID) { //dont send INVALID packets
		return;
	}
	p.type = t;
	p.data = data;
	ecrobot_send_bt_packet((void *) &p, sizeof(struct bt_packet));
}

BtPacket read_bt_packet() {
	static BtPacket p;
	if(!ecrobot_read_bt_packet((void *)&p, sizeof(struct bt_packet))) { //if no new data has been received set packet INVALID
		p.type = INVALID;
	}
	return p;
}

#endif
