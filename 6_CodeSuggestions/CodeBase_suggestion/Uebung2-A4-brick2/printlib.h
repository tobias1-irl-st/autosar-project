#ifndef PRINTLIB_H
#define PRINTLIB_H

#define DISPLAY_ROWS 7

static int dispYpos = 0;

void printStringNumber(const char *s, int number)
{	
	if(!dispYpos)
		display_clear(1);
	display_goto_xy(0,dispYpos);
	display_string(s);
	display_int(number, 3);
	display_update();
	if(++dispYpos > DISPLAY_ROWS) {
		dispYpos = 0;
	}
} 

void printNumber(int number)
{		
	if(!dispYpos)
		display_clear(1);
	display_goto_xy(0,dispYpos);
	display_int(number, 3);
	display_update();
	if(++dispYpos > DISPLAY_ROWS) {
		dispYpos = 0;
	}
} 

void printString(const char *s)
{		
	if(!dispYpos)
		display_clear(1);
	display_goto_xy(0,dispYpos);
	display_string(s);
	display_update();
	if(++dispYpos > DISPLAY_ROWS) {
		dispYpos = 0;
	}
}

#endif
