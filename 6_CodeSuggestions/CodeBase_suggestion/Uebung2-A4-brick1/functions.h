static int direction = 25;

void user_1ms_isr_type2(void){ 
	StatusType ercd;
	ercd = SignalCounter(Ctr_1ms);
	if( ercd != E_OK ){
		ShutdownOS( ercd );
	}
} 

void ecrobot_device_initialize()
{
	printString("wait...");
	U8 slaveAddr[7] = {0,22,83,23,12,15,0};
	ecrobot_init_bt_master(slaveAddr,"AUTOSAR_VL");
	printString("connected");
}

int ReadPort_speed()
{
	return direction;
}

void WriteTriggerPort_speed()
{
	//dont do anything..
	//send_bt_packet(EVENT, 1);
}

void WritePort_speed(int number)
{
	direction = number;	
	send_bt_packet(SPEED_DATA, number);
}

void WaitTriggerPort_speed()
{
	WaitEvent(evt_disp_speed);
	ClearEvent(evt_disp_speed);
}

void WaitTriggerPort_leftBtn()
{
	WaitEvent(evt_poll_left);
	ClearEvent(evt_poll_left);
}

void WriteTriggerPort_leftBtn()
{
	SetEvent(ReverseDir, evt_poll_left);
}

void WaitTriggerPort_rightBtn()
{
	WaitEvent(evt_poll_right);
	ClearEvent(evt_poll_right);
}

void WriteTriggerPort_rightBtn()
{
	SetEvent(Drive, evt_poll_right);
}
