#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h" 
#include "printlib.h"
#include "btlib.h" 

DeclareTask(Drive);
DeclareTask(ReverseDir);
DeclareTask(PollRight); 
DeclareTask(PollLeft);

DeclareCounter(Ctr_1ms);

DeclareEvent(evt_poll_right);
DeclareEvent(evt_poll_left);
DeclareEvent(evt_disp_speed);

DeclareAlarm(al_poll_right);
DeclareAlarm(al_poll_left);

#include "functions.h"

TASK(PollRight)
{
	static int btn_drive_status = 0, btn_drive_old = 0;
	
	btn_drive_status = ecrobot_get_touch_sensor(NXT_PORT_S4);
	
	if(!btn_drive_old && btn_drive_status) {
		WriteTriggerPort_rightBtn();
		//printString("Button Drive pushed!");
	}

	btn_drive_old = btn_drive_status;
	TerminateTask();
}

TASK(PollLeft)
{
	static int btn_direction_status = 0, btn_direction_old = 0;
	btn_direction_status = ecrobot_get_touch_sensor(NXT_PORT_S1);
	
	if(!btn_direction_old && btn_direction_status) {
		WriteTriggerPort_leftBtn();
		//printString("evt direction set!");
	}

	btn_direction_old = btn_direction_status;
	TerminateTask();
}

TASK(Drive)
{
	//printString("Drive activated!");
	while(1) {
		//printString("Wait drvbtn!");
		WaitTriggerPort_rightBtn();
		//printString("Driving!");
		
		ecrobot_set_motor_speed(NXT_PORT_A, direction); /* Motor an mit 50% Power */
		ecrobot_set_motor_speed(NXT_PORT_C, direction); /* Motor an mit 50% Power */
		
		systick_wait_ms(1000); /* 1000ms warten */
		
		ecrobot_set_motor_speed(NXT_PORT_A,0); /* Motor aus */
		ecrobot_set_motor_speed(NXT_PORT_C,0); /* Motor aus */	
		
		ClearEvent(evt_poll_right);
	}
	TerminateTask(); /* Task beenden */
} 

TASK(ReverseDir)
{
	while(1) {
		WaitTriggerPort_leftBtn();
		//printString("changing direction");
		WritePort_speed(ReadPort_speed()*-1);
		WriteTriggerPort_speed();
		ClearEvent(evt_poll_left);
	}
	TerminateTask();
}
