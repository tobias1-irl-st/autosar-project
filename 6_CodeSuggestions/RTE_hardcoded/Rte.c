// TODO: declare OSEK stuff
/*
// Tasks with their events:
JoystickTask
Rte_Ev_TM_Cyclic_100ms // Alarm

LogicTask
Rte_Ev_DR_DriveInfo_EngineSpeed_Left
Rte_Ev_DR_DriveInfo_EngineSpeed_Right
Rte_Ev_DR_JoystickInfo_DirectionX
Rte_Ev_DR_JoystickInfo_DirectionY

DriveTask
Rte_Ev_DR_DriveCtrl_DirectionX
Rte_Ev_DR_DriveCtrl_DirectionY

AudioTask
Rte_Ev_DR_PlayAudio_AudioFrequency 
Rte_Ev_DR_PlayAudio_PeepFrequency 
Rte_Ev_DR_PlayAudio_PeepEnable 

DisplayTask
Rte_Ev_DR_PrintData_DirectionString
Rte_Ev_DR_PrintData_Speed
*/

#include "Rte_Types.h"

/*********************************************************************
 * Rte global variables
 *********************************************************************/
int8_t Rte_Logic_PrintData_Speed;
displayLineString Rte_Logic_PrintData_DirectionString;
uint8_t Rte_Logic_PlayAudio_AudioFrequency;
uint8_t Rte_Logic_PlayAudio_PeepFrequency;
uint8_t Rte_Logic_PlayAudio_PeepEnable;
int32_t Rte_Drive_DriveInfo_EngineSpeed_Left;
int32_t Rte_Drive_DriveInfo_EngineSpeed_Right;
int8_t Rte_Logic_DriveCtrl_DirectionX;
int8_t Rte_Logic_DriveCtrl_DirectionY;
int8_t Rte_Joystick_JoystickInfo_DirectionX;
int8_t Rte_Joystick_JoystickInfo_DirectionY;

/*********************************************************************
 * Rte_Read functions
 *********************************************************************/
void Rte_Read_Display_PrintData_DirectionString(displayLineString* data) {
	memcpy(data, Rte_Logic_PrintData_DirectionString, sizeof(displayLineString));
}

/*********************************************************************
 * Rte_Write functions
 *********************************************************************/
void Rte_Write_Logic_PrintData_DirectionString(const displayLineString data) {
	memcpy(Rte_Logic_PrintData_DirectionString, data, sizeof(displayLineString));
	SetEvent(DisplayTask, Rte_Ev_DR_PrintData_DirectionString);
}

/*********************************************************************
 * Runnables
 *********************************************************************/
extern void DisplayDrawDirection();
extern void DisplayDrawSpeed();
extern void JoystickCyclic();

/*********************************************************************
 * Rte Tasks
 *********************************************************************/
TASK(AudioTask) {		
	while (1) {
		WaitEvent(ev1 | ev2 ..);

		EventMaskType event;
		GetEvent(AudioTask, &event);
		ClearEvent(event);
		
		// TODO: For every runnable specify an entrypoint dependent on their triggers
		if (event & (ev1 | ev2 ..)) {

		}
		if (event & (ev1 | ev4 ..)) {

		}
	}
	TerminateTask();
}

TASK(DisplayTask) {	
	while (1) {
		WaitEvent(Rte_Ev_DR_PrintData_DirectionString | Rte_Ev_DR_PrintData_Speed);

		EventMaskType event;
		GetEvent(DisplayTask, &event);
		ClearEvent(event);
		
		if (event & (Rte_Ev_DR_PrintData_DirectionString)) {
			DisplayDrawDirection();
		}
		if (event & (Rte_Ev_DR_PrintData_Speed)) {
			DisplayDrawSpeed();
		}
	}
	TerminateTask();
}

TASK(DriveTask) {	
	while (1) {
		WaitEvent(ev1 | ev2 ..);

		EventMaskType event;
		GetEvent(DriveTask, &event);
		ClearEvent(event);
		
		// TODO: For every runnable specify an entrypoint dependent on their triggers
		if (event & (ev1 | ev2 ..)) {

		}
		if (event & (ev1 | ev4 ..)) {

		}
	}
	TerminateTask();
}

TASK(JoystickTask) {	
	while (1) {
		WaitEvent(Rte_Ev_TM_Cyclic_100ms);

		EventMaskType event;
		GetEvent(JoystickTask, &event);
		ClearEvent(event);
		
		if (event & (Rte_Ev_TM_Cyclic_100ms)) {
			JoystickCyclic();
		}
	}
	TerminateTask();
}

TASK(LogicTask) {	
	while (1) {
		WaitEvent(ev1 | ev2 ..);

		EventMaskType event;
		GetEvent(LogicTask, &event);
		ClearEvent(event);
		
		// TODO: For every runnable specify an entrypoint dependent on their triggers
		if (event & (ev1 | ev2 ..)) {

		}
		if (event & (ev1 | ev4 ..)) {

		}
	}
	TerminateTask();
}

