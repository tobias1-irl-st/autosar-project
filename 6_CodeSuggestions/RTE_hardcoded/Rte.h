#ifndef RTE_h
#define RTE_h
// TODO: Some Display ports, Maybe some IRVs

#include "Rte_Types.h"
#include "btlib.h"


/*********************************************************************
 * Rte global variables
 *********************************************************************/
extern int8_t Rte_Logic_DriveCtrl_DirectionX;
extern int8_t Rte_Logic_DriveCtrl_DirectionY;
extern int32_t Rte_Drive_DriveInfo_EngineSpeed_Left;
extern int32_t Rte_Drive_DriveInfo_EngineSpeed_Right;
extern int8_t Rte_Joystick_JoystickInfo_DirectionX;
extern int8_t Rte_Joystick_JoystickInfo_DirectionY;
extern uint8_t Rte_Logic_PlayAudio_AudioFrequency;
extern uint8_t Rte_Logic_PlayAudio_PeepEnable;
extern uint8_t Rte_Logic_PlayAudio_PeepFrequency;
extern displayLineString Rte_Logic_PrintData_DirectionString;
extern int8_t Rte_Logic_PrintData_Speed;

/*********************************************************************
 * Rte communication functions
 *********************************************************************/
extern void Rte_Read_Display_PrintData_DirectionString(displayLineString* data);
extern void Rte_Write_PrintData_DirectionString(const displayLineString data);

/*********************************************************************
 * Rte_Read Receivers
 *********************************************************************/
#define Rte_Read_DriveCtrl_DirectionX(data) (*(data) = Rte_Logic_DriveCtrl_DirectionX) // for Drive
#define Rte_Read_DriveCtrl_DirectionY(data) (*(data) = Rte_Logic_DriveCtrl_DirectionY) // for Drive
#define Rte_Read_DriveInfo_EngineSpeed_Left(data) (*(data) = Rte_Drive_DriveInfo_EngineSpeed_Left) // for Logic
#define Rte_Read_DriveInfo_EngineSpeed_Right(data) (*(data) = Rte_Drive_DriveInfo_EngineSpeed_Right) // for Logic
#define Rte_Read_JoystickInfo_DirectionX(data) (*(data) = Rte_Joystick_JoystickInfo_DirectionX) // for Logic
#define Rte_Read_JoystickInfo_DirectionY(data) (*(data) = Rte_Joystick_JoystickInfo_DirectionY) // for Logic
#define Rte_Read_PlayAudio_AudioFrequency(data) (*(data) = Rte_Logic_PlayAudio_AudioFrequency) // for Audio
#define Rte_Read_PlayAudio_PeepEnable(data) (*(data) = Rte_Logic_PlayAudio_PeepEnable) // for Audio
#define Rte_Read_PlayAudio_PeepFrequency(data) (*(data) = Rte_Logic_PlayAudio_PeepFrequency) // for Audio
#define Rte_Read_PrintData_DirectionString Rte_Read_Display_PrintData_DirectionString // for Display
#define Rte_Read_PrintData_Speed(data) (*(data) = Rte_Logic_PrintData_Speed) // for Display

/*********************************************************************
 * Rte_Write Senders
 *********************************************************************/
#define Rte_Write_DriveCtrl_DirectionX(data) (Rte_Logic_DriveCtrl_DirectionX = (data), SetEvent(DriveTask, Rte_Ev_DR_DriveCtrl_DirectionX)) // for Logic
#define Rte_Write_DriveCtrl_DirectionY(data) (Rte_Logic_DriveCtrl_DirectionY = (data), SetEvent(DriveTask, Rte_Ev_DR_DriveCtrl_DirectionY)) // for Logic
#define Rte_Write_DriveInfo_EngineSpeed_Left(data) (Rte_Drive_DriveInfo_EngineSpeed_Left = (data), SetEvent(LogicTask, Rte_Ev_DR_DriveInfo_EngineSpeed_Left)) // for Drive
#define Rte_Write_DriveInfo_EngineSpeed_Right(data) (Rte_Drive_DriveInfo_EngineSpeed_Right = (data), SetEvent(LogicTask, Rte_Ev_DR_DriveInfo_EngineSpeed_Left)) // for Drive
#define Rte_Write_JoystickInfo_DirectionX(data) (Rte_Joystick_JoystickInfo_DirectionX = (data), SetEvent(LogicTask, Rte_Ev_DR_JoystickInfo_DirectionX)) // for Joystick
#define Rte_Write_JoystickInfo_DirectionY(data) (Rte_Joystick_JoystickInfo_DirectionY = (data), SetEvent(LogicTask, Rte_Ev_DR_JoystickInfo_DirectionY)) // for Joystick
#define Rte_Write_PlayAudio_AudioFrequency(data) (Rte_Logic_PlayAudio_AudioFrequency = (data), SetEvent(AudioTask, Rte_Ev_DR_PlayAudio_AudioFrequency)) // for Logic
#define Rte_Write_PlayAudio_PeepEnable(data) (Rte_Logic_PlayAudio_PeepEnable = (data), SetEvent(AudioTask, Rte_Ev_DR_PlayAudio_PeepEnable)) // for Logic
#define Rte_Write_PlayAudio_PeepFrequency(data) (Rte_Logic_PlayAudio_PeepFrequency = (data), SetEvent(AudioTask, Rte_Ev_DR_PlayAudio_PeepFrequency)) // for Logic
#define Rte_Write_PrintData_DirectionString Rte_Write_Logic_PrintData_DirectionString // for Logic
#define Rte_Write_PrintData_Speed(data) (Rte_Logic_PrintData_Speed = (data), SetEvent(DisplayTask, Rte_Ev_DR_PrintData_Speed)) // for Logic



/***
HW
*/
#define Rte_IO_MotorLinks(name) (name)

#endif
