#include "kernel.h"
#include "kernel_id.h"
#include "ecrobot_interface.h"

void myPrintln(const char *s)
{
static int y = 0;
display_goto_xy(0,y);
display_string(s);
display_update();
y++;
}

void user_1ms_isr_type2(void)
{
}

DeclareTask(Main);
DeclareTask(Disp);
DeclareEvent(evt_dir);

int y = 0;
int dir = 0;

void myPrint(const char *s)
{
   display_goto_xy(0,y);
   display_string(s);
   display_update();
}

void WaitTriggerPort_direction() {

	int n = 0;
	while(n==0) {
		n = ecrobot_read_bt_packet((U8 *)&dir, sizeof(dir));
		/*display_goto_xy(1, 5);
		display_int(n, 3);
		display_goto_xy(1, 6);
		display_int(dir, 3);
		display_update();*/
	}
}

int ReadPort_direction() {
	return dir;
}
/*
void ecrobot_device_initialize()
{
	ecrobot_init_bt_slave("AUTOSAR_VL");
}
*/
TASK(Disp)
{
	while(1) {
		WaitTriggerPort_direction();
		if (ReadPort_direction()==1) myPrintln("vorwaerts");
		else myPrintln("rueckwaerts");
	}
}

TASK(Main)
{
    U8 data[7];
	
	if (!ecrobot_get_bt_device_address(data))
	{
	   myPrint("failure");
	}
	else
	{
	   for(int f=0;f<7;f++)
	   {
	      display_goto_xy(0,y);
		  display_int(data[f],0);
		  display_update();
		  y++;
	   }
	} 

	TerminateTask();
}

