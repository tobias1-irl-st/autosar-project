/**
 */
package dasa.tests;

import dasa.Brick;
import dasa.DASAsystem;
import dasa.DasaFactory;
import dasa.DasaPackage;
import dasa.generators.bluetoothgenerator.BluetoothGenerator;
import dasa.generators.oilgenerator.OILGenerator;
import dasa.generators.rtegenerator.RteGenerator;
import dasa.generators.taskgen.TaskGenerator;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.common.util.Diagnostic;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.util.Diagnostician;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

/**
 * <!-- begin-user-doc -->
 * A sample utility for the '<em><b>dasa</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class DasaExample {
	/**
	 * <!-- begin-user-doc -->
	 * Load all the argument file paths or URIs as instances of the model.
	 * <!-- end-user-doc -->
	 * @param args the file paths or URIs.
	 * @generated NOT
	 */
	public static void main(String[] args) {
		// Create a resource set to hold the resources.
		//
		ResourceSet resourceSet = new ResourceSetImpl();

		// Register the appropriate resource factory to handle all file extensions.
		//
		resourceSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put
			(Resource.Factory.Registry.DEFAULT_EXTENSION,
			 new XMIResourceFactoryImpl());

		// Register the package to ensure it is available during loading.
		//
		resourceSet.getPackageRegistry().put
			(DasaPackage.eNS_URI,
			 DasaPackage.eINSTANCE);

		// If there are no arguments, emit an appropriate usage message.
		//
		File file = new File("..\\..\\runtime-EclipseApplication\\BaseModel\\Editor\\LegoSAR.dasa");
		URI uri = null;

		try {
			uri = URI.createFileURI(file.getCanonicalPath());
		} catch (IOException e) {
			System.out.println("ERROR: requested file not found!");
			e.printStackTrace();
		}

        try {
            // Demand load resource for this file.
            //
            Resource resource = resourceSet.getResource(uri, true);
            System.out.println("Loaded " + uri);

            // Getting the object 'DASAsystem' with all the data created in editor
            DASAsystem dasaObject = (DASAsystem)resource.getContents().get(0);
            File rootPath = new File(dasaObject.getRootPath().replaceAll("%User%", System.getProperty("user.name")));
            if(!rootPath.exists()) {
            	rootPath.mkdirs();
            }

            // YOUR CODE HERE!
            // work with the object 'DASAsystem' and find all your neccessary data for your code generation :-)
            // a quick example:

            new RteGenerator().generate(dasaObject, rootPath);
            new TaskGenerator().generate(dasaObject, rootPath);
            new OILGenerator().generate(dasaObject, rootPath);
            new BluetoothGenerator().generate(dasaObject, rootPath);




        }
        catch (RuntimeException exception) {
            System.out.println("Problem loading " + uri);
            exception.printStackTrace();
        }
	}

	/**
	 * <!-- begin-user-doc -->
	 * Prints diagnostics with indentation.
	 * <!-- end-user-doc -->
	 * @param diagnostic the diagnostic to print.
	 * @param indent the indentation for printing.
	 * @generated
	 */
	protected static void printDiagnostic(Diagnostic diagnostic, String indent) {
		System.out.print(indent);
		System.out.println(diagnostic.getMessage());
		for (Diagnostic child : diagnostic.getChildren()) {
			printDiagnostic(child, indent + "  ");
		}
	}

} //DasaExample
