package dasa.generators.bluetoothgenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.AbstractSet;
import java.util.LinkedList;
import java.util.List;

import org.eclipse.emf.common.util.EList;

import dasa.Brick;
import dasa.Component;
import dasa.DASAsystem;
import dasa.DataReceivedEvent;
import dasa.OSEKSystem;
import dasa.PortConnection;
import dasa.ReadSWPort;
import dasa.SWPort;
import dasa.generators.CommonFunctions;
import dasa.generators.IGenerator;

public class BluetoothGenerator implements IGenerator {
	private String masterFileString;
	private String slaveFileString;
	private Brick masterBrick;
	private Brick slaveBrick;
	private EList<PortConnection> conns;
	private EList<Component> components;
	DASAsystem ds;

	@Override
	public void generate(DASAsystem ds, File root) {
		System.out.println("Bluetooth Generation Started");
		System.out.println(root.getPath());
		this.ds = ds;
		masterFileString = generateIncludes();
		slaveFileString = generateIncludes();
		conns = ds.getPorconnections();
		components = ds.getComponents();

		for(Brick b : ds.getBricks()) {
			if(b.isBtMaster()) {
				masterBrick = b;
			} else {
				slaveBrick = b;
			}
		}
		// Generate File Stubs
		if (ds.getBricks().size() == 2) {
//			masterBrick = ds.getBricks().get(0);
//			slaveBrick = ds.getBricks().get(1);
			masterFileString += generateSlaveAddr(slaveBrick.getBtAddress());
			slaveFileString += generateSlaveAddr(masterBrick.getBtAddress());
		} else {
			System.out.println("ACHTUNG ACHTUNG KEINE 2 BRICKS IN VERWENDUNG, STOPPE BT GENERIERUNG.");
			return;
			//throw new RuntimeException("Communication only supports exactly two devices, but " + ds.getBricks().size() + " devices found in object model");
		}

		masterFileString += generateStartFunctions(true);
		slaveFileString += generateStartFunctions(false);

		generateOutputFiles(root, masterFileString, slaveFileString);

		System.out.println("Bluetooth Generation done");

	}

	private void generateOutputFiles(File root_path, String masterSource,
			String slaveSource) {
		for (Brick b : this.ds.getBricks()) {
			File path = new File(root_path, b.getName());
			if (!path.exists()) {
				path.mkdirs();
			}

			File headerFile = new File(path, "btlib.h");
			File masterSourceFile = new File(path, "bluetooth_master.c");
			File slaveSourceFile = new File(path, "bluetooth_slave.c");

			try {
				FileWriter headerFW = new FileWriter(
						headerFile.getAbsoluteFile());
				headerFW.write(generateH(b));
				headerFW.close();

				FileWriter masterSourceFW = new FileWriter(
						masterSourceFile.getAbsoluteFile());
				masterSourceFW.write(masterSource);
				masterSourceFW.close();

				FileWriter slaveSourceFW = new FileWriter(
						slaveSourceFile.getAbsoluteFile());
				slaveSourceFW.write(slaveSource);
				slaveSourceFW.close();

			} catch (IOException e) {
				System.out.println("ERROR: printing files crashed!");
				e.printStackTrace();
			}
		}

	}

	private String generateH(Brick b) {
		String ret = "#include \"Rte.h\"\n\n"
				+ "\nvoid beginConnectionSlave() {\n"
				+ "    while (ecrobot_get_bt_status() != BT_STREAM) {\n"
				+ "      ecrobot_init_bt_slave(\"1234\");\n"
				+ "    }\n"
				+ "}\n\n"
				+"void beginConnectionMaster(U8 slaveAddr[7])\n\n"
				+ "{\n"
				+ "\twhile(ecrobot_get_bt_status() != BT_STREAM)\n"
				+ "\tecrobot_init_bt_master(slaveAddr, \"1234\"); //Maximal 16 Zeichen!\n"
				+ "}\n\n"
				+ "\ntypedef struct bt_packet {\n"
				+ "  uint16_t id;\n"
				+ "  uint8_t length;\n"
				+ "  uint32_t payload;\n"
				+ "} BtPacket;\n"
				+ generateBtSend(b)
				+ "\nBtPacket read_bt_packet() {\n"
				+ "  static BtPacket p;\n"
				+ "	 ecrobot_read_bt_packet((void *) &p, sizeof(struct bt_packet));\n"
				+ "  return p;\n"
				+ "}\n\n";

		ret += generateBtRecvData(b);
		return ret;
	}

	private String generateBtSend(Brick b) {
		String s = "void btSendData(uint16_t id, uint32_t payload, uint8_t length) {\n"
				+ "%s"
				+ "  static BtPacket p;\n"
				+ "  p.id = id;\n"
				+ "  p.length = length;\n"
				+ "  p.payload = payload;\n"
				+ "%s"
				+ "\t\tsystick_wait_ms(10);\n"
				+ "\t\tecrobot_send_bt_packet((void *) &p, sizeof(struct bt_packet));\n"
				+ "}\n";

		String cache = "";
		String cacheCheck = "";
		for(PortConnection c : conns) {
			cache += "\tstatic " + c.getDatatype() + " old_" + c.getId() + " = 0;\n";
			cacheCheck += "\tif((id == " + c.getId() + ") && (((" + c.getDatatype() + ") payload) != old_" + c.getId() + ")) {\n"
					+ "\t\t old_" + c.getId() +" = payload;\n"
					+ "\t} else ";
		}

		cacheCheck += "{ return; }\n";

		return String.format(s, cache, cacheCheck);
	}

	private String generateBtRecvData(Brick b) {
		String cache = "";
		String ret = "" + "void btRecvData() {\n" + "  BtPacket p;\n"
				+ "  p.length = 0;\n" + "  p = read_bt_packet();\n"
				+ cache
				+ "  if(p.length > 0) {\n" + "    switch (p.id) {\n";
//		List<String> cases = CommonFunctions.generateWriteFunctionStrings(ds);
//		for(String s : cases) {
//			ret += s;
//		}
		for (PortConnection conn : conns) {
			ret += "    case " + conn.getId() + ":\n";
			Brick fromWritePort = CommonFunctions.getBrickFromPort(conn.getWrite(), ds);

			for (ReadSWPort swp : conn.getRead()) {
				//Brick fromReadPort = CommonFunctions.getBrickFromPort(swp, ds);
				if(b.equals(fromWritePort)) {
					continue;
				}
				if(CommonFunctions.isPortOnSameBrick(swp, conn.getWrite(), ds)) {
					System.out.println(swp.getName() + " is on same brick as " + conn.getWrite().getName());
					continue;
				} else {
					System.out.println(swp.getName() + " is NOT on same brick as " + conn.getWrite().getName());

				}
				ret += "\t\tRte_Write_BtRcvData_" + CommonFunctions.getComponentNameFor(conn.getWrite(), ds) + "_"+ conn.getWrite().getName()
						+ "((" + conn.getDatatype() +") p.payload);\n";

			}
			ret += "      break;\n";

		}

		ret += "    }\n  }\n}\n";
		return ret;
	}

	private String generateStartFunctions(boolean master) {
		String hook = "void ecrobot_device_initialize() {\n";
				if(master)
				{
					hook += "\t\tU8 slave_addr[7] = SLAVEADDR;\n"
							+"\t\tbeginConnectionMaster(slave_addr);\n";
				}
				else
				{
					hook +="\t\tbeginConnectionSlave();\n";
				}
				hook += "}\n";
		String ret = "\nvoid Bluetooth() {\n"
				+"\tbtRecvData();\n"
				+"}\n";
		return hook + ret;
	}

	private String generateSlaveAddr(String slave) {
		String[] byteStrs = slave.split(":");
		String ret = "#define SLAVEADDR {";
		for(int i = 0; i < byteStrs.length - 1; i++) {
			ret += byteStrs[i] + ", ";
		}
		ret += byteStrs[byteStrs.length - 1];
//		for (String byteStr : byteStrs) {
//			ret += byteStr;
//			if (!byteStr.equals(byteStrs[byteStrs.length - 1])) {
//				ret += ",";
//			}
//		}
		ret += "}\n";
		return ret;
	}

	private String generateIncludes() {
		String ret = "" + "#include \"kernel.h\"\n"
				+ "#include \"kernel_id.h\"\n"
				+ "#include \"ecrobot_interface.h\"\n"
				+ "#include \"btlib.h\"\n" + "#include <stdint.h>\n";
		return ret;
	}

}
