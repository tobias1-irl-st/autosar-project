package dasa.generators;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.eclipse.emf.common.util.EList;

import dasa.Brick;
import dasa.Component;
import dasa.DASAsystem;
import dasa.DataReceivedEvent;
import dasa.Event;
import dasa.OSEKSystem;
import dasa.PortConnection;
import dasa.ReadSWPort;
import dasa.Runnable;
import dasa.SWPort;
import dasa.Task;
import dasa.TimeEvent;
import dasa.WriteSWPort;
import dasa.generators.rtegenerator.FunctionSelector;
import dasa.generators.rtegenerator.ReceiverRunnableDestinationInfo;
import dasa.generators.rtegenerator.RteCCodeGenerator;
import dasa.generators.rtegenerator.TaskBrickInformation;

/**
 *
 * @author stt32353 Tobias Straubinger
 *
 */
public class CommonFunctions {

    /**
     * Returns component name for contained SWPort
     *
     * @param SWPort
     * @param DASAsystem
     * @return String
     */

    private static Map<SWPort, String> compStrCache = new HashMap<>();
    private static Map<SWPort, Component> compCache = new HashMap<>();
    private static Map<Event, String> taskCache = new HashMap<>();
    private static Map<SWPort, Brick> brickCache = new HashMap<>();

    public static String getComponentNameFor(SWPort p, DASAsystem d) {
        if (compStrCache.containsKey(p))
            return compStrCache.get(p);
        for (Component c : d.getComponents()) {
            if (c.getSwports().contains(p)) {
                compStrCache.put(p, c.getName());
                return c.getName();
            }
        }
        throw new RuntimeException("Port " + p.getName()
                + " not found in any component! ABORTING, please check model");
    }

    public static Component getComponentFor(SWPort p, DASAsystem d) {
        if (compCache.containsKey(p))
            return compCache.get(p);
        for (Component c : d.getComponents()) {
            if (c.getSwports().contains(p)) {
                compCache.put(p, c);
                return c;
            }
        }
        throw new RuntimeException("Port " + p.getName()
                + " not found in any component! ABORTING, please check model");
    }

    /**
     * Returns task name for contained Event
     *
     * @param SWPort
     * @param DASAsystem
     * @return String
     */
    public static String getTaskNameFor(Event e, DASAsystem d) {
        if (taskCache.containsKey(e))
            return taskCache.get(e);
        for (Brick b : d.getBricks()) {
            for (Task t : b.getOs().getTasks()) {
                for (Runnable r : t.getTaskrunnables()) {
                    if (r.getEvents().contains(e)) {
                        taskCache.put(e, t.getName());
                        return t.getName();
                    }
                }
            }
        }
        throw new RuntimeException(
                "Event not found in any task! ABORTING, please check model");
    }

    public static LinkedList<String> generateWriteFunctionStrings(DASAsystem ds) {
        LinkedList<String> retList = new LinkedList<>();

        for (PortConnection pc : ds.getPorconnections()) {
            LinkedList<ReceiverRunnableDestinationInfo> rcvDataList = new LinkedList<>();
            LinkedList<ReceiverRunnableDestinationInfo> rcvBTList = new LinkedList<>();
            String writeCmpntName = "";
            String writePortName = "";
            String dataType = "";
            WriteSWPort currentWritePort = pc.getWrite();
            EList<ReadSWPort> currentReadPorts = pc.getRead();

            String writerBrickName = "";
            for (Component cmpnt : ds.getComponents()) {
                for (SWPort swPort : cmpnt.getSwports()) {
                    if (swPort instanceof WriteSWPort) {
                        if (currentWritePort.getName().equals(swPort.getName())) {
                            LinkedList<TaskBrickInformation> writeInfo = new LinkedList<>();
                            // Aktuelle Komponente ist writer der aktuellen
                            // PortConnection
                            writeCmpntName = cmpnt.getName();
                            writePortName = swPort.getName();
                            dataType = pc.getDatatype();
                            writeInfo = getTasksForComponent(ds, cmpnt);
                            writerBrickName = writeInfo.get(0).getBrickName();
                        }
                    }
                }
            }

            for (Component cmpnt : ds.getComponents()) {
                for (SWPort swPort : cmpnt.getSwports()) {
                    if (swPort instanceof ReadSWPort) {
                        if (currentReadPorts.contains(swPort)) {
                            // Aktuelle Komponente ist reader bei der aktuellen
                            // PortConnection
                            for (TaskBrickInformation tbi : getTasksForComponent(
                                    ds, cmpnt)) {
                                int btId = -1;
                                if (!writerBrickName.equals(tbi.getBrickName())) {
                                    // �ber Bluetooth senden
                                    btId = pc.getId();
                                    boolean found = false;
                                    for (ReceiverRunnableDestinationInfo info : rcvDataList) {
                                        if (info.getBtId() == btId) {
                                            found = true;
                                        }
                                    }
                                    if (!found) {
                                        rcvDataList
                                                .add(new ReceiverRunnableDestinationInfo(
                                                        tbi.getTaskName(),
                                                        cmpnt.getName(), swPort
                                                                .getName(),
                                                        btId));
                                        rcvBTList
                                                .add(new ReceiverRunnableDestinationInfo(
                                                        tbi.getTaskName(),
                                                        cmpnt.getName(), swPort
                                                                .getName(), -1));
                                    }
                                } else {
                                    LinkedList<Task> readPortTasks = getTasksForReadSWPortAtBrick(
                                            ds, (ReadSWPort) swPort,
                                            tbi.getBrick());
                                    for (Task t : readPortTasks) {
                                        ReceiverRunnableDestinationInfo rrdi = new ReceiverRunnableDestinationInfo(
                                                t.getName(), cmpnt.getName(),
                                                swPort.getName(), btId);
                                        if (!rcvDataList.contains(rrdi)) {
                                            rcvDataList.add(rrdi);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            if (rcvBTList.size() == 1) {
                // retList.add(RteCCodeGenerator.generateHeaderBluetoothWriteDefineFunction(writeCmpntName,
                // writePortName));
                retList.add(
                          "    case " + pc.getId() + ":\n"
                        + "      Rte_Write_BtRcvData_" + writeCmpntName + "_" + writePortName + "((" + pc.getDatatype() + ")" + pc.getId() + ");\n"
                        + "      break;\n");
            } else if(rcvBTList.size() > 1) {
                String s ="    case " + pc.getId() + ":\n";

                for(ReceiverRunnableDestinationInfo rrdi : rcvBTList) {
                    s += "      Rte_Write_BtRcvData_" + rrdi.getRcvComponent() + "_" + rrdi.getRcvPort() + "((" + pc.getDatatype() + ")" + pc.getId() + ");\n";
                }
                s += "      break;\n";
            }
        }
        return retList;
    }

    private static LinkedList<TaskBrickInformation> getTasksForComponent(
            DASAsystem ds, Component cmpnt) {
        String brick;
        LinkedList<TaskBrickInformation> result = new LinkedList<>();
        for (Component c : ds.getComponents()) {
            if (c.getName().equals(cmpnt.getName())) {
                for (dasa.Runnable cRunnable : c.getRunnables()) {
                    for (Brick b : ds.getBricks()) {
                        OSEKSystem osek = b.getOs();
                        for (Task t : osek.getTasks()) {
                            for (dasa.Runnable tRunnable : t.getTaskrunnables()) {
                                if (cRunnable.getName().equals(
                                        tRunnable.getName())) {
                                    TaskBrickInformation tmp_Tbi = new TaskBrickInformation(
                                            t, b);
                                    if (!result.contains(tmp_Tbi)) {
                                        result.add(tmp_Tbi);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if (!result.isEmpty()) {
            brick = result.get(0).getBrickName();
            for (TaskBrickInformation info : result) {
                if (!info.getBrickName().equals(brick)) {
                    System.out.println("Error: Komponente " + cmpnt.getName()
                            + " ist auf mehrere Bricks verteilt!");
                }
            }
        }
        return result;
    }

    private static LinkedList<Task> getTasksForReadSWPortAtBrick(DASAsystem ds,
            ReadSWPort rswp, Brick brick) {
        LinkedList<Task> tasklist = new LinkedList<>();
        LinkedList<dasa.Runnable> tmp_rSwUsed = new LinkedList<>();

        for (Component cmpnt : ds.getComponents()) {
            for (SWPort swp : cmpnt.getSwports()) {
                if (swp == rswp) {
                    for (dasa.Runnable r : cmpnt.getRunnables()) {
                        for (Event e : r.getEvents()) {
                            if (e instanceof DataReceivedEvent) {
                                for (ReadSWPort p : ((DataReceivedEvent) e)
                                        .getEventports()) {
                                    if (p == rswp) {
                                        tmp_rSwUsed.add(r);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        OSEKSystem os = brick.getOs();
        for (Task t : os.getTasks()) {
            for (dasa.Runnable r : t.getTaskrunnables()) {
                if (tmp_rSwUsed.contains(r)) {
                    if (!tasklist.contains(t)) {
                        tasklist.add(t);
                    }
                }
            }
        }

        return tasklist;
    }

    private static Map<Brick, List<Runnable>> rfbCache = new HashMap<>();
    public static List<Runnable> getRunnablesFromBrick(Brick b) {
        if(rfbCache.containsKey(b)) {
            return rfbCache.get(b);
        }
        List<Runnable> rs = new LinkedList<>();
        for(Task t : b.getOs().getTasks()) {
            rs.addAll(t.getTaskrunnables());
        }
        rfbCache.put(b, rs);
        return rs;
    }

    private static Map<PortConnection, List<Runnable>> rfpcCache = new HashMap<>();
    public static List<Runnable> getRunnablesFromPortConnection(PortConnection pc, DASAsystem ds) {
        if(rfpcCache.containsKey(pc)) {
        	return rfpcCache.get(pc);
        }
        List<Runnable> rs = new LinkedList<>();
        /* Add Runnables from WritePort */
        rs.addAll(getComponentFor(pc.getWrite(), ds).getRunnables());

        /* Add Runnables from Readports */
        for(SWPort p : pc.getRead()) {
            rs.addAll(getComponentFor(p, ds).getRunnables());
        }

        rfpcCache.put(pc, rs);
        return rs;
    }

    public static List<Runnable> getRunnablesFromPort(SWPort p, DASAsystem ds) {
    	List<Runnable> rs = new LinkedList<>();
        rs.addAll(getComponentFor(p, ds).getRunnables());
        return rs;
    }

    public static Brick getBrickFromPort(SWPort p, DASAsystem ds) {
    	//teilmenge zu runnables von port und runnables von brick -> Port an Brick
    	Set<Runnable> portRunnables = new HashSet();
    	Set<Runnable> brickRunnables = new HashSet();
    	portRunnables.addAll(getRunnablesFromPort(p, ds));
    	for (Brick b : ds.getBricks()) {
    		brickRunnables.clear();
    		brickRunnables.addAll(getRunnablesFromBrick(b));
    		if (!Collections.disjoint(brickRunnables, portRunnables)) {
    			return b;
    		}
    	}
    	throw new RuntimeException("Could not determine brick for port " + p.getName());
    }

    public static boolean isPortOnSameBrick(SWPort p1, SWPort p2, DASAsystem ds) {
    	return getBrickFromPort(p1, ds).equals(getBrickFromPort(p2, ds));
    }
}
