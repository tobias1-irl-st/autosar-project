package dasa.generators;

import java.io.File;

import dasa.DASAsystem;

public interface IGenerator {

	void generate(DASAsystem ds, File root);

}
