package dasa.generators.oilgenerator;

import java.io.File;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.*;

import dasa.*;
import dasa.Runnable;
import dasa.generators.IGenerator;
import dasa.generators.taskgen.TaskGenerator;



public class OILGenerator implements IGenerator {
	static private String appmode = "appmode0";
	static private int alarmIndex = 0;
	static private String separator = "************************************************************************";
	private DASAsystem ds;
	@Override
	public void generate(DASAsystem ds, File root)
	{
		this.ds = ds;
		//HEADER
		//foreach brick
		//	->OSEK-System
		//	foreach task
		//		�ber runnables alle events holen
		//		code generieren
		// 		events einf�gen
		System.out.println("OILGenerator started.");
		//if (!root.exists())
		//	root.mkdirs();
		try{
			File srcDir = new File(root.getCanonicalPath() + "\\Runnables");
			if (!srcDir.exists())
			{
				srcDir.mkdirs();
				System.out.println("Created Runnables Folder.");
			}
			System.out.println("Put your C sourcefiles in the Runnables Folder.");
			System.out.println(separator);
		}
		catch(Exception e)
		{
			System.out.println(e.getMessage());
		}
		for (Brick b : ds.getBricks())
		{
			try
			{
				WriteBrickOIL(b, root);
			}
			catch (IOException e)
			{
				System.out.print(e.getMessage());
			}
		}

	}

	public static class FL {
	    public static boolean flag;
	}

	private void GenerateMakefile(Brick b, File root) throws IOException
	{
		File makefile = new File(root.getCanonicalPath() + "\\" + "Makefile");
		FileWriter fw = new FileWriter(makefile);
		PrintWriter pw = new PrintWriter(fw);

		Set<String> sourceFiles = new HashSet<>();
		for(Task t : b.getOs().getTasks()) {
			for(Runnable r : t.getTaskrunnables()) {
				sourceFiles.add(r.getSourceFile());
			}
		}

		String target = "TARGET = " + root.getName();
		String prefix = "../Runnables/";
		String target_src = "TARGET_SOURCES = ";
		for(String s : sourceFiles) {
			if(!(s == null || s.isEmpty())){
				target_src += prefix + s + " ";
			}
		}
		target_src += TaskGenerator.taskImplFileName
				+ " Rte.c "
				+ (ds.getBricks().size() > 1 ? (b.isBtMaster() ? "bluetooth_master.c" : "bluetooth_slave.c") : "")
				+ "\n";

		String oil = "TOPPERS_OSEK_OIL_SOURCE := ./" + b.getName() + ".oil";
		String o_path = "O_PATH ?= build";
		String include = "include ../../../nxtOSEK/ecrobot/ecrobot.mak";


		pw.println(target);
		pw.println(target_src);
		pw.println(oil);
		pw.println(o_path);
		pw.println(include);
		pw.close();
	}

	private void WriteBrickOIL(Brick b, File f) throws IOException
	{
		System.out.println("Generating OIL-File for Brick" + b.getName() + ".");
		File brickDir = new File(f.getCanonicalPath() + "\\" + b.getName());
		if(!brickDir.exists())
			brickDir.mkdirs();
		String filename = brickDir + "\\" + b.getName() + ".oil";

		//String filename = "G:/Test/" + "_" + b.getName() + ".oil";
		AbstractSet<Event> events = getAllEvents(b);
		AbstractSet<Integer> counters = new HashSet<Integer>();
		FL.flag = false;
		FileWriter fw = new FileWriter(filename);
		PrintWriter writer = new PrintWriter(fw);
		//List<Event> alreadyGenerated = new LinkedList<Event>();
		List<String> alreadyGenerated_DR = new LinkedList<String>();
		List<String> alreadyGenerated_times = new LinkedList<String>();
		List<String> alreadyGenerated_T = new LinkedList<String>();
		writer.print(InitOIL());
		writer.print("{");
		writer.print("\t" + generateHeader(b));
		for (Task t : b.getOs().getTasks())
		{
			for(Runnable runnable : t.getTaskrunnables())
			{
				for(Event e : runnable.getEvents())
				{
					//if (!alreadyGenerated.contains(e)){
						writer.print("\t" + generateEvent(e, t, counters, alreadyGenerated_DR, alreadyGenerated_times, alreadyGenerated_T));
					//	alreadyGenerated.add(e);
					//}
				}
			}
			writer.print("\t" + generateTask(t));
		}

		writer.print("};");
		writer.close();
		System.out.println("OIL-File for Brick" + b.getName() + "generated.");
		GenerateMakefile(b, brickDir);
		System.out.println("Makfile generated for " + b.getName());
		System.out.println(separator);
		alreadyGenerated_DR.clear();
		alreadyGenerated_T.clear();
		alreadyGenerated_times.clear();
	}
	private AbstractSet<Event> getAllEvents(Brick b)
	{
		AbstractSet<Event> events = new HashSet<Event>();
		for (Task t : b.getOs().getTasks())
		{
			for (Runnable r : t.getTaskrunnables())
			{
				for (Event e : r.getEvents())
				{
					if (!events.contains(e))
						events.add(e);
				}
			}
		}
		return events;
	}
	private String generateHeader(Brick b)
	{
		String header =
				"\n\tOS LEJOS_OSEK\n"
				+ "\t{\n"
				+ "\t\tSTATUS = " + b.getOs().getStatus() + ";\n"
				+ "\t\tSTARTUPHOOK = " + fix(b.getOs().isStartupHook()) + ";\n"
				+ "\t\tERRORHOOK = " + fix(b.getOs().isErrorHook()) + ";\n"
				+ "\t\tSHUTDOWNHOOK = " + fix(b.getOs().isShutdownHook()) + ";\n"
				+ "\t\tPRETASKHOOK = " + fix(b.getOs().isPreTaskHook()) + ";\n"
				+ "\t\tPOSTTASKHOOK = " + fix(b.getOs().isPostTaskHook()) + ";\n"
				+ "\t\tUSEGETSERVICEID = " + fix(b.getOs().isUseGetServiceId()) + ";\n"
				+ "\t\tUSEPARAMETERACCESS = " + fix(b.getOs().isUserParameterAccess()) + ";\n"
				+ "\t\tUSERESSCHEDULER = " + fix(b.getOs().isUseResScheduler()) + ";\n"
				+ "\t};\n"
				+ "\tAPPMODE " + appmode + "{};\n";

		return String.format(header);
	}

	private String fix(boolean b) {
		return String.valueOf(b).toUpperCase();
	}

	private String generateTask(Task t)
	{
		List<Runnable> runnables = t.getTaskrunnables();
		List<Event> events = new LinkedList<Event>();
		String body = "";
		for (Runnable r : runnables)
		{
			events.addAll(r.getEvents());
		}
		String head = "\n\tTASK " + t.getName() + "\n";
		body = body + "\t{\n"
				+ "\t\tAUTOSTART = " + (t.isAutostart() ? "TRUE\n\t\t" : "FALSE;\n")
				+ AppMode(t.isAutostart())
				+ "\t\tPRIORITY = " + String.valueOf(t.getPriority()) + ";\n"
				+ "\t\tSCHEDULE = " + t.getSchedule() + ";\n"
				+ "\t\tSTACKSIZE = " + String.valueOf(t.getStacksize()) + ";\n"
				+ "\t\tACTIVATION = " + t.getActivation() + ";\n"
				+ Events(events)
				+ "\t};\n";
		System.out.println("Task " + t.getName() + "generated");
		return head + body;
	}


	private String AppMode(boolean autostart)
	{
		if (autostart)
		{
			return "{\n\t\t" + "\tAPPMODE = " + appmode + ";\n\t\t};\n";
		}
		else return "";
	}

	private String Events(List<Event> events)

	{
		String body = "";
		for (Event e : events)
		{
			AbstractSet<String> event_name = getEventName(e);
			for(String bodys : event_name)
			{
				body = body + ("\t\tEVENT = " + bodys + ";\n");
			}

		}
		return body;

	}

	private AbstractSet<String> getEventName(Event e){
		AbstractSet<String> event_name =  new HashSet<String>();;
		if (e instanceof TimeEvent)
		{
			int time = ((TimeEvent) e).getTime();
			event_name.add("Rte_Ev_TM_Cyclic_" + time);
		}
		if (e instanceof DataReceivedEvent)
		{
			List<ReadSWPort> port_ev = ((DataReceivedEvent) e).getEventports();
			List<Component> components = ds.getComponents();
			//event_name = ((ReadSWPort) port_ev).getName();
			for (SWPort port_sw : port_ev)
			{
				for (Component c : components)
				{
					for (SWPort port_sw_c : c.getSwports())
					{
						if (port_sw_c instanceof ReadSWPort)
						{
							if(port_sw_c == port_sw) {
								event_name.add("Rte_Ev_DR_"+c.getName() + "_"+ port_sw.getName());
							}
						}
					}
				}
			}
		}
		return event_name;
	}
	private String generateEvent(Event e, Task t, AbstractSet<Integer> counters, List<String> alreadyGenerated_DR, List<String> alreadyGenerated_times, List<String> alreadyGenerated_T)
	{
		AbstractSet<String> event_name = getEventName(e);
		String body = "";
		for(String bodys : event_name)
		{
			if (!alreadyGenerated_DR.contains(bodys))
			{
				body = body + "\n\tEVENT " + bodys + "\n\t{\n"
							+ "\t\tMASK = AUTO;\n"
								+ "\t};\n";
				alreadyGenerated_DR.add(bodys);
			}


				if (e instanceof TimeEvent )
				{
					//if(!alreadyGenerated_T.contains(bodys))
					//{

						//if (!alreadyGenerated_times.contains(bodys))
						//{
							body = body + (generateAlarm(t, (TimeEvent)e, event_name, counters, alreadyGenerated_times));
							alreadyGenerated_T.add(bodys);
						//}
					//}
				}
		}
		System.out.println("Event " + event_name + " generated.");
		return body;
	}

	private String generateAlarm(Task t, TimeEvent myEvent, AbstractSet<String> event_name, AbstractSet<Integer> counters, List<String> alreadyGenerated_times)
	{
		String body = "";

		int maxAllowedValue = myEvent.getTime();
		if (!FL.flag)
		{
			body = body + generateCounter(myEvent);
			FL.flag = true;
		}
		String alarm_name = "";
		for(String evt : event_name)
		{
			alarm_name = "al_" + maxAllowedValue + "_ms_" + evt + "_" + t.getName();
			if (!alreadyGenerated_times.contains(alarm_name))
			{
				String ctr_name = "Ctr_1_ms";

				body = body + "\n\tALARM " + alarm_name + "\n\t{\n"
						+ "\t\tCOUNTER = " + ctr_name + ";\n"
						+ "\t\tACTION = SETEVENT \n\t\t{\n"
						+ "\t\t\tTASK = " + t.getName() + ";\n"
						+ "\t\t\tEVENT = " + evt + ";\n\t\t};\n"
						+ "\t\tAUTOSTART = TRUE\n" //evtl bool mit in die klasse aufnehmen
						+ "\t\t{\n\t\t\tALARMTIME = 1;\n"
						+ "\t\t\tCYCLETIME = " + String.valueOf(maxAllowedValue) + ";\n"
						+ "\t\t\tAPPMODE = " + appmode + ";\n\t\t};\n\t};\n";
				alreadyGenerated_times.add(alarm_name);
			}
		}
		System.out.println("Alarm " + alarm_name + "generated.");
		return body;
	}

	private String generateCounter(TimeEvent myEvent)
	{
		String body = "";
		//int maxAllowedValue = myEvent.getTime();

		String name = "Ctr_1_ms";

		body = "\n\tCOUNTER " + name + "\n\t{\n"
				+ "\t\tMINCYCLE = 1;\n"
				+ "\t\tMAXALLOWEDVALUE = 10000;\n"
				+ "\t\tTICKSPERBASE = 1;\n"
				+ "\t};\n";
		System.out.println("Counter " + name + "generated.");
		return body;
	}

	private String InitOIL()
	{
		return "#include \"implementation.oil\"\nCPU ATMEL_AT91SAM7S256\n";
	}
	}
