package dasa.generators.rtegenerator;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import dasa.generators.taskgen.TaskGenerator;

public class RteCCodeGenerator {

	/*********************************************************************
	 * SOURCE generate functions
	 *********************************************************************/
	// Source: start
	public static String generateSourceRteStart() {
		String ret = "";

		String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		ret += "/*********************************************************************\n"
				+ " * Rte source file\n"
				+ " * Time generated: " + date + "\n"
				+ " *********************************************************************/\n"
				+ "#include \"Rte.h\"\n";
		return ret;
	}
	static String generateSourceRteEnd() {
		String ret = "";
		return ret;
	}

	// Source: variable
	static String generateSourceVariableStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte global variables\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateSourceVariable(String datatype, String writeComponent, String writePortName) {
		String ret = datatype + " Rte_" + writeComponent + "_" + writePortName + ";\n";
		return ret;
	}

	// Source: write function
	static String generateSourceWriteFunctionStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte_Write functions\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateSourceWriteFunction(String datatype, String writeComponent, String writePortName, List<ReceiverRunnableDestinationInfo> portList) {
		String ret = "void Rte_Write_" + writeComponent + "_" + writePortName + "( " + datatype + " data) {\n";

		boolean genLocal = false;
		boolean genBt = false;
		String setEvents = "";
		int btId = -1;
		for (ReceiverRunnableDestinationInfo rcvData : portList) {
			if (rcvData.getBtId() == -1) {
				genLocal = true;
				setEvents += "    SetEvent(" + rcvData.getTask() + ", Rte_Ev_DR_" + rcvData.getRcvComponent() + "_" + rcvData.getRcvPort() + ");\n";
			} else {
				btId = rcvData.getBtId();
				genBt = true;
			}
		}

		if (genLocal) {
			ret += "    Rte_" + writeComponent + "_" + writePortName + " = data;\n";
			ret += setEvents;
		}

		if (genLocal && genBt) {
			ret += "\n";
		}

		if (genBt) {
			ret += "    btSendData(" + btId + ", (uint32_t) data, sizeof(uint32_t));\n";
		}

		ret += "}\n\n";
		return ret;
	}

	static String generateSourceBluetoothWriteFunction(String datatype, String writeComponent, String writePortName, List<ReceiverRunnableDestinationInfo> portList) {
		String ret = "void Rte_Write_BtRcvData_" + writeComponent + "_" + writePortName + "( " + datatype + " data) {\n";

		boolean genLocal = false;
		String setEvents = "";
		int btId = -1;
		for (ReceiverRunnableDestinationInfo rcvData : portList) {
			genLocal = true;
			setEvents += "    SetEvent(" + rcvData.getTask() + ", Rte_Ev_DR_" + rcvData.getRcvComponent() + "_" + rcvData.getRcvPort() + ");\n";
		}

		if (genLocal) {
			ret += "    Rte_" + writeComponent + "_" + writePortName + " = data;\n";
			ret += setEvents;
		}

		ret += "\n}\n\n";
		return ret;
	}


	/*********************************************************************
	 * HEADER generate functions
	 *********************************************************************/
	// Header: start
	static String generateHeaderRteStart() {
		String ret = "";

		String date = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss").format(new Date());
		ret += "/*********************************************************************\n"
				+ " * Rte header file\n"
				+ " * Time generated: " + date + "\n"
				+ " *********************************************************************/\n\n"

				+ "#ifndef RTE_h\n"
				+ "#define RTE_h\n\n"
				+ ""
				+ "#include \"ecrobot_interface.h\"\n"
				+ "#include <stdint.h>\n"
				+ "#include \"" + TaskGenerator.taskHeaderFileName + "\"\n"
				+ "\n"
				+ "extern void btSendData(uint16_t, uint32_t, uint8_t);\n";
		// TODO: includes
		return ret;
	}
	static String generateHeaderRteEnd() {
		String ret = "#endif\n";
		return ret;
	}

	// Header: variable
	static String generateHeaderVariableStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte global variables\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateHeaderVariable(String datatype, String writeComponent, String writePortName) {
		String ret = "extern " + datatype + " Rte_" + writeComponent + "_" + writePortName + ";\n";
		return ret;
	}

	// Header: write function declaration
	static String generateHeaderWriteFunctionDeclarationStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte write function declaration\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateHeaderWriteFunctionDeclaration(String datatype, String writeComponent, String writePortName) {
		String ret = "extern void Rte_Write_" + writeComponent + "_" + writePortName + "( " + datatype + " data);\n";
		return ret;
	}

	static String generateHeaderBluetoothWriteFunctionDeclaration(String datatype, String writeComponent, String writePortName) {
		String ret = "extern void Rte_Write_BtRcvData_" + writeComponent + "_" + writePortName + "( " + datatype + " data);\n";
		return ret;
	}

	// Header: read define
	static String generateHeaderReadDefineStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte_Read Receivers\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateHeaderReadDefine(String readComponent, String readPortName, String writeComponent, String writePortName) {
		String ret = "#define Rte_Read_" + readComponent + "_" + readPortName + "(data) (*(data) = Rte_" + writeComponent + "_" + writePortName + ")\n";
		return ret;
	}

	// Header: write define
	static String generateHeaderWriteDefineStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte_Write Senders\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateHeaderWriteDefine(String datatype, String writeComponent, String writePortName, ReceiverRunnableDestinationInfo port) {
		String ret = "#define Rte_Write_" + writeComponent + "_" + writePortName + "(data) (";

		if (port.getBtId() == -1) {
			ret += "Rte_" + writeComponent + "_" + writePortName + " = (data), ";
			ret += "SetEvent(" + port.getTask() + ", Rte_Ev_DR_" + port.getRcvComponent() + "_" + port.getRcvPort() + "))\n";
		} else {
			ret += "btSendData(" + port.getBtId() + ", (uint32_t)(data), sizeof(uint32_t)))\n";
		}

		return ret;
	}

	static String generateHeaderBluetoothDefine(String datatype, String writeComponent, String writePortName, ReceiverRunnableDestinationInfo port) {
		String ret = "#define Rte_Write_BtRcvData_" + writeComponent + "_" + writePortName + "(data) (";

		ret += "Rte_" + writeComponent + "_" + writePortName + " = (data), ";
		ret += "SetEvent(" + port.getTask() + ", Rte_Ev_DR_" + port.getRcvComponent() + "_" + port.getRcvPort() + "))\n";

		return ret;
	}

	static String generateHeaderWriteDefineFunction(String writeComponent, String writePortName) {
		String ret = "#define Rte_Write_" + writeComponent + "_" + writePortName + " Rte_Write_" + writeComponent + "_" + writePortName + "\n";
		return ret;
	}

	static String generateHeaderBluetoothWriteDefineFunction(String writeComponent, String writePortName) {
		String ret = "#define Rte_Write_BtRcvData_" + writeComponent + "_" + writePortName + " Rte_Write_" + writeComponent + "_" + writePortName + "\n";
		return ret;
	}

	// Header: HardwareIO
	static String generateHeaderHwInOutStart() {
		String ret = "\n"
				+ "/*********************************************************************\n"
				+ " * Rte hardware in/out\n"
				+ " *********************************************************************/\n";
		return ret;
	}
	static String generateHeaderHwInOut(String hardwareName, String brickPort) {
		String ret = "#define Rte_IO_" + hardwareName + " (" + brickPort + ")\n";
		return ret;
	}

	static String generateHeaderHwInOutI2C(String hardwareName, int I2CAdress) {
		String ret = "#define Rte_IO_" + hardwareName + "_Address" + " (" + I2CAdress + ")\n";
		return ret;
	}
}
