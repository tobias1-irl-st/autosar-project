package dasa.generators.rtegenerator;

import java.util.LinkedList;
import java.util.List;

public class RteCCodeGeneratorTest {
	public static void main(String [] args) {
		String datatype = "int";
		String writeComponent = "Drive";
		String writePortName = "DriveCtrl_DirectionX";

		String readComponent = "Logic";
		String readPortName = "Drehzahl";

		ReceiverRunnableDestinationInfo rcvBt = new ReceiverRunnableDestinationInfo("TestTask", "Display", "PrintLn", 420);
		ReceiverRunnableDestinationInfo rcvNonBt1 = new ReceiverRunnableDestinationInfo("TestTask", readComponent, readPortName, -1);
		ReceiverRunnableDestinationInfo rcvNonBt2 = new ReceiverRunnableDestinationInfo("TestTask", "Logic", "Verbrauchs_Duesen_Oeffnungs_Spalt_Groesse", -1);
		List<ReceiverRunnableDestinationInfo> portList = new LinkedList<ReceiverRunnableDestinationInfo>();
		portList.add(rcvBt);
		portList.add(rcvNonBt1);
		portList.add(rcvNonBt2);

		String hardwareName = "MotorLinks";
		String brickPort = "A";





		System.out.print(RteCCodeGenerator.generateSourceRteStart());

		System.out.print(RteCCodeGenerator.generateSourceRteEnd());

		// Source: variable
		System.out.print(RteCCodeGenerator.generateSourceVariableStart());

		System.out.print(RteCCodeGenerator.generateSourceVariable( datatype,  writeComponent,  writePortName));


		// Source: write function
		System.out.print(RteCCodeGenerator.generateSourceWriteFunctionStart());

		System.out.print(RteCCodeGenerator.generateSourceWriteFunction( datatype,  writeComponent,  writePortName, portList));


		/*********************************************************************
		 * HEADER generate functions
		 *********************************************************************/
		// Header: start
		System.out.print(RteCCodeGenerator.generateHeaderRteStart());

		System.out.print(RteCCodeGenerator.generateHeaderRteEnd());

		// Header: variable
		System.out.print(RteCCodeGenerator.generateHeaderVariableStart());

		System.out.print(RteCCodeGenerator.generateHeaderVariable( datatype,  writeComponent,  writePortName));

		// Header: write function declaration
		System.out.print(RteCCodeGenerator.generateHeaderWriteFunctionDeclarationStart());

		System.out.print(RteCCodeGenerator.generateHeaderWriteFunctionDeclaration( datatype,  writeComponent,  writePortName));


		// Header: read define
		System.out.print(RteCCodeGenerator.generateHeaderReadDefineStart());

		System.out.print(RteCCodeGenerator.generateHeaderReadDefine( readComponent,  readPortName,  writeComponent,  writePortName));

		// Header: write define
		System.out.print(RteCCodeGenerator.generateHeaderWriteDefineStart());

		System.out.print(RteCCodeGenerator.generateHeaderWriteDefine( datatype,  writeComponent,  writePortName, rcvNonBt2));
		System.out.print(RteCCodeGenerator.generateHeaderWriteDefine( datatype,  writeComponent,  writePortName, rcvBt));

		System.out.print(RteCCodeGenerator.generateHeaderWriteDefineFunction( writeComponent,  writePortName));

		// Header: HardwareIO
		System.out.print(RteCCodeGenerator.generateHeaderHwInOutStart());

		System.out.print(RteCCodeGenerator.generateHeaderHwInOut( hardwareName,  brickPort));
	}

}
