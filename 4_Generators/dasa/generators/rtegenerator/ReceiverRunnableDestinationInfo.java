package dasa.generators.rtegenerator;

public class ReceiverRunnableDestinationInfo {
	private String task;
	private String rcvComponent;
	private String rcvPort;
	private int btId;

	public ReceiverRunnableDestinationInfo(String task, String rcvComponent, String rcvPort, int btId) { //  ReceiverRunnableDestinationInfo
		super();
		this.task = task;
		this.rcvComponent = rcvComponent;
		this.rcvPort = rcvPort;
		this.btId = btId;
	}
	public String getTask() {
		return task;
	}
	public void setTask(String task) {
		this.task = task;
	}
	public String getRcvComponent() {
		return rcvComponent;
	}
	public void setRcvComponent(String rcvComponent) {
		this.rcvComponent = rcvComponent;
	}
	public String getRcvPort() {
		return rcvPort;
	}
	public void setRcvPort(String rcvPort) {
		this.rcvPort = rcvPort;
	}
	public int getBtId() {
		return btId;
	}
	public void setBtId(int btId) {
		this.btId = btId;
	}

	@Override
	public boolean equals(Object other) {
		boolean ret = false;
		if(other instanceof ReceiverRunnableDestinationInfo) {
			if(this.task.equals(((ReceiverRunnableDestinationInfo) other).getTask())
				&& this.rcvComponent.equals(((ReceiverRunnableDestinationInfo) other).getRcvComponent())
				&& this.rcvPort.equals(((ReceiverRunnableDestinationInfo) other).rcvPort)
				&& this.btId == ((ReceiverRunnableDestinationInfo) other).getBtId())
			{
				ret = true;
			}
		}
		return ret;
	}
}
