package dasa.generators.rtegenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Collection;
import java.util.LinkedList;

import org.eclipse.emf.common.util.EList;

import dasa.Brick;
import dasa.Component;
import dasa.DASAsystem;
import dasa.DataReceivedEvent;
import dasa.Event;
import dasa.I2CSensor;
import dasa.OSEKSystem;
import dasa.PortConnection;
import dasa.ReadSWPort;
import dasa.SWPort;
import dasa.SensorActor;
import dasa.Task;
import dasa.WriteSWPort;
import dasa.generators.IGenerator;

public class RteGenerator implements IGenerator {
	private DASAsystem ds;

	@Override
	public void generate(DASAsystem ds, File root_path) {
		this.ds = ds;
		System.out.println("\n\nRTE-GENERATOR STARTED!");
		System.out.println("generating rte...");

		for(Brick b: this.ds.getBricks()) {
			LinkedList<String> source = generateSource(b);
			LinkedList<String> header = generateHeader(b);
			System.out.println("printing RTE for " + b.getName() + " ...");
			this.generateOutputFiles(root_path, b.getName(), source, header);
		}


		System.out.println("RTE-Generator finished!\n\n");
	}

	private void generateOutputFiles(File root_path, String brickName, LinkedList<String> source, LinkedList<String> header) {
			File path = new File(root_path, brickName);
			if (!path.exists()) {
				path.mkdirs();
			}

			File headerFile = new File(path, "Rte.h");
			File sourceFile = new File(path, "Rte.c");

			try {
				FileWriter headerFW = new FileWriter(headerFile.getAbsoluteFile());
				for(String s : header) { headerFW.write(s); }
				headerFW.close();

				FileWriter sourceFW = new FileWriter(sourceFile.getAbsoluteFile());
				for(String s : source) { sourceFW.write(s);	}
				sourceFW.close();

			} catch (IOException e) {
				System.out.println("ERROR: printing files crashed!");
				e.printStackTrace();
			}

	}

	private LinkedList<String> generateHeader(Brick b) {
		LinkedList<String> retList = new LinkedList<>();
		FunctionSelector fs = FunctionSelector.HEADERDECLARATION;

		retList.add(RteCCodeGenerator.generateHeaderRteStart());

		retList.add(RteCCodeGenerator.generateHeaderVariableStart());
		retList.addAll(this.generateVariableStrings(fs));

		retList.add(RteCCodeGenerator.generateHeaderWriteFunctionDeclarationStart());
		retList.addAll(this.generateWriteFunctionStrings(b, fs));

		retList.add(RteCCodeGenerator.generateHeaderReadDefineStart());
		retList.addAll(this.generateReadDefineStrings());

		retList.add(RteCCodeGenerator.generateHeaderWriteDefineStart());
		retList.addAll(this.generateWriteFunctionStrings(b, FunctionSelector.HEADERDEFINE));

		retList.add(RteCCodeGenerator.generateHeaderHwInOutStart());
		retList.addAll(this.generateHwInOutStrings());

		retList.add(RteCCodeGenerator.generateHeaderRteEnd());

		return retList;
	}

	private LinkedList<String> generateSource(Brick b) {
		LinkedList<String> retList = new LinkedList<>();
		FunctionSelector fs = FunctionSelector.SOURCE;

		retList.add(RteCCodeGenerator.generateSourceRteStart());

		retList.add(RteCCodeGenerator.generateSourceVariableStart());
		retList.addAll(this.generateVariableStrings(fs));

		retList.add(RteCCodeGenerator.generateSourceWriteFunctionStart());
		retList.addAll(this.generateWriteFunctionStrings(b, fs));

		retList.add(RteCCodeGenerator.generateSourceRteEnd());

		return retList;
	}

	private LinkedList<String> generateHwInOutStrings() {
		LinkedList<String> retList = new LinkedList<>();
		for(Brick b : this.ds.getBricks()) {
			for (SensorActor sa : b.getSensoractors()) {
				retList.add(RteCCodeGenerator.generateHeaderHwInOut(sa.getName(), sa.getNXTport()));
				if(sa instanceof I2CSensor) {
					retList.add(RteCCodeGenerator.generateHeaderHwInOutI2C(sa.getName(), ((I2CSensor) sa).getI2caddress()));
				}
			}
		}
		return retList;
	}

	private Collection<? extends String> generateReadDefineStrings() {
		LinkedList<String> retList = new LinkedList<>();
		for(PortConnection pc : this.ds.getPorconnections()) {
			String writeSwPortName = pc.getWrite().getName();
			String writeComponent = "";

			for(Component cpmnt : this.ds.getComponents()) {
				for(SWPort swport : cpmnt.getSwports()) {
					if (swport.getName().equals(writeSwPortName)) {
						writeComponent = cpmnt.getName();
					}
				}
			}

			for(Component cmpnt : this.ds.getComponents()) {
				for(SWPort swPort : cmpnt.getSwports()) {
					for(ReadSWPort readswport : pc.getRead()) {
						if(swPort.getName().equals(readswport.getName())) {
							String gen_def = RteCCodeGenerator.generateHeaderReadDefine(cmpnt.getName(), readswport.getName(), writeComponent, writeSwPortName);
							if(!retList.contains(gen_def)) {
								retList.add(gen_def);
							}
						}
					}

				}
			}
		}
		return retList;
	}

	private LinkedList<String> generateWriteFunctionStrings(Brick b, FunctionSelector fs) {
		LinkedList<String> retList = new LinkedList<>();

		for(PortConnection pc : this.ds.getPorconnections()) {
			LinkedList<ReceiverRunnableDestinationInfo> rcvDataList = new LinkedList<>();
			LinkedList<ReceiverRunnableDestinationInfo> rcvBTList = new LinkedList<>();
			String writeCmpntName = "";
			String writePortName = "";
			String dataType = "";
			WriteSWPort currentWritePort = pc.getWrite();
			EList<ReadSWPort> currentReadPorts = pc.getRead();

			String writerBrickName = "";
			for(Component cmpnt : this.ds.getComponents()) {
				for(SWPort swPort : cmpnt.getSwports()) {
					if(swPort instanceof WriteSWPort) {
						if(currentWritePort.getName().equals(swPort.getName())) {
							LinkedList<TaskBrickInformation> writeInfo = new LinkedList<>();
							// Aktuelle Komponente ist writer der aktuellen PortConnection
							writeCmpntName = cmpnt.getName();
							writePortName = swPort.getName();
							dataType = pc.getDatatype();
							writeInfo = getTasksForComponent(cmpnt);
							writerBrickName = writeInfo.get(0).getBrickName();
						}
					}
				}
			}



			for(Component cmpnt : this.ds.getComponents()) {
				for(SWPort swPort : cmpnt.getSwports()) {
					if(swPort instanceof ReadSWPort) {
						if (currentReadPorts.contains(swPort)) {
							// Aktuelle Komponente ist reader bei der aktuellen PortConnection
							for(TaskBrickInformation tbi : getTasksForComponent(cmpnt)) {
								int btId = -1;
								if(!writerBrickName.equals(tbi.getBrickName())) {
									// �ber Bluetooth senden
									btId = pc.getId();
									boolean found = false;
									for(ReceiverRunnableDestinationInfo info : rcvDataList) {
										if(info.getBtId() == btId) {
											found = true;
										}
									}
									if(!found) {
										rcvDataList.add(new ReceiverRunnableDestinationInfo(tbi.getTaskName(), cmpnt.getName(), swPort.getName(), btId));
										rcvBTList.add(new ReceiverRunnableDestinationInfo(tbi.getTaskName(), cmpnt.getName(), swPort.getName(), -1));
									}
								} else {
									LinkedList<Task> readPortTasks = getTasksForReadSWPortAtBrick((ReadSWPort) swPort, tbi.getBrick());
									for(Task t : readPortTasks) {
										ReceiverRunnableDestinationInfo rrdi = new ReceiverRunnableDestinationInfo(t.getName(), cmpnt.getName(), swPort.getName(), btId);
										if(!rcvDataList.contains(rrdi)) {
											rcvDataList.add(rrdi);
										}
									}
								}
							}
						}
					}
				}
			}

			if (b.getName() == writerBrickName) {
				if(FunctionSelector.SOURCE == fs) {
					if(rcvDataList.size() > 1) {
						retList.add(RteCCodeGenerator.generateSourceWriteFunction(dataType, writeCmpntName, writePortName, rcvDataList));
					}
				} else if(FunctionSelector.HEADERDECLARATION == fs) {
					if(rcvDataList.size() > 1) {
						retList.add(RteCCodeGenerator.generateHeaderWriteFunctionDeclaration(dataType, writeCmpntName, writePortName));
					}
				} else if(FunctionSelector.HEADERDEFINE == fs) {
					if(rcvDataList.size() == 1) {
						retList.add(RteCCodeGenerator.generateHeaderWriteDefine(dataType, writeCmpntName, writePortName, rcvDataList.get(0)));
					} else if(rcvDataList.size() > 1) {
						retList.add(RteCCodeGenerator.generateHeaderWriteDefineFunction(writeCmpntName, writePortName));
					} else {
						throw new RuntimeException("Missing Receiver Runnable for Port " + writePortName);
					}
				}
			}
			else {
				if(FunctionSelector.SOURCE == fs) {
					if(rcvBTList.size() > 1) {
						retList.add(RteCCodeGenerator.generateSourceBluetoothWriteFunction(dataType, writeCmpntName, writePortName, rcvBTList));
					}
				} else if(FunctionSelector.HEADERDECLARATION == fs) {
					if(rcvBTList.size() > 1) {
						retList.add(RteCCodeGenerator.generateHeaderBluetoothWriteFunctionDeclaration(dataType, writeCmpntName, writePortName));
					}
				} else if(FunctionSelector.HEADERDEFINE == fs) {
					if(rcvBTList.size() == 1) {
						retList.add(RteCCodeGenerator.generateHeaderBluetoothDefine(dataType, writeCmpntName, writePortName, rcvBTList.get(0)));
					} else if (rcvBTList.size() >1) {
						retList.add(RteCCodeGenerator.generateHeaderBluetoothWriteDefineFunction(writeCmpntName, writePortName));
					}
				}
			}

		}
		return retList;
	}

	private LinkedList<String> generateVariableStrings(FunctionSelector fs) {
		LinkedList<String> retList = new LinkedList<>();

		for(PortConnection pc : this.ds.getPorconnections()) {
			WriteSWPort currentWritePort = pc.getWrite();

			for(Component cmpnt : this.ds.getComponents()) {
				for(SWPort swPort : cmpnt.getSwports()) {
					if(swPort instanceof WriteSWPort) {
						if(currentWritePort.getName().equals(swPort.getName())) {
							// Aktuelle Komponente ist writer der aktuellen PortConnection
							String writeCmpntName = cmpnt.getName();
							String writePortName = swPort.getName();
							String dataType = pc.getDatatype();
							if(fs == FunctionSelector.SOURCE) {
								retList.add(RteCCodeGenerator.generateSourceVariable(dataType, writeCmpntName, writePortName));
							} else if(fs == FunctionSelector.HEADERDECLARATION){
								retList.add(RteCCodeGenerator.generateHeaderVariable(dataType, writeCmpntName, writePortName));
							}
						}
					}
				}
			}
		}
		return retList;
	}

	private LinkedList<TaskBrickInformation> getTasksForComponent(Component cmpnt) {
		String brick;
		LinkedList<TaskBrickInformation> result = new LinkedList<>();
		for(Component c : this.ds.getComponents()) {
			if(c.getName().equals(cmpnt.getName())) {
				for(dasa.Runnable cRunnable : c.getRunnables()) {
					for(Brick b : this.ds.getBricks()) {
						OSEKSystem osek = b.getOs();
						for(Task t : osek.getTasks()) {
							for(dasa.Runnable tRunnable : t.getTaskrunnables()) {
								if(cRunnable.getName().equals(tRunnable.getName())) {
									TaskBrickInformation tmp_Tbi = new TaskBrickInformation(t, b);
									if(!result.contains(tmp_Tbi)) {
										result.add(tmp_Tbi);
									}
								}
							}
						}
					}
				}
			}
		}
		if(!result.isEmpty()) {
			brick = result.get(0).getBrickName();
			for(TaskBrickInformation info : result) {
				if(!info.getBrickName().equals(brick)) {
					System.out.println("Error: Komponente " + cmpnt.getName() + " ist auf mehrere Bricks verteilt!");
				}
			}
		}
		return result;
	}

	private LinkedList<Task> getTasksForReadSWPortAtBrick(ReadSWPort rswp, Brick brick) {
		LinkedList<Task> tasklist = new LinkedList<>();
		LinkedList<dasa.Runnable> tmp_rSwUsed = new LinkedList<>();


		for(Component cmpnt : this.ds.getComponents()) {
			for(SWPort swp : cmpnt.getSwports()) {
				if(swp == rswp) {
					for(dasa.Runnable r : cmpnt.getRunnables()) {
						for(Event e : r.getEvents()) {
							if(e instanceof DataReceivedEvent) {
								for(ReadSWPort p : ((DataReceivedEvent) e).getEventports()) {
									if(p == rswp) {
										tmp_rSwUsed.add(r);
									}
								}
							}
						}
					}
				}
			}
		}

		OSEKSystem os = brick.getOs();
		for(Task t : os.getTasks()) {
			for(dasa.Runnable r : t.getTaskrunnables()) {
				if(tmp_rSwUsed.contains(r)) {
					if(!tasklist.contains(t)) {
						tasklist.add(t);
					}
				}
			}
		}

		return tasklist;
	}
}
