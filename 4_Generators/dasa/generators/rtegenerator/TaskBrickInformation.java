package dasa.generators.rtegenerator;

import dasa.Brick;
import dasa.Task;

public class TaskBrickInformation {
	private Task task;
	private Brick brick;

	public TaskBrickInformation(Task t, Brick b) {
		this.task = t;
		this.brick = b;
	}

	public Task getTask() {
		return task;
	}

	public void setTask(Task task) {
		this.task = task;
	}

	public Brick getBrick() {
		return brick;
	}

	public void setBrick(Brick brick) {
		this.brick = brick;
	}

	public String getBrickName() {
		return this.brick.getName();
	}

	public String getTaskName() {
		return this.task.getName();
	}

	@Override
	public boolean equals(Object other){
		boolean ret = false;
		if(other instanceof TaskBrickInformation) {
			if(this.getBrickName().equals(((TaskBrickInformation) other).getBrickName())) {
				if(this.getTaskName().equals(((TaskBrickInformation) other).getTaskName())) {
					ret = true;
				}
			}
		}
		return ret;
	}
}
