package dasa.generators.taskgen;

/**
 * Task-Generator Autoren:
 * Stefan Wopperer
 * Tobias Straubinger
 */

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.LinkedList;

import dasa.Brick;
import dasa.Component;
import dasa.DASAsystem;
import dasa.DataReceivedEvent;
import dasa.Event;
import dasa.OSEKSystem;
import dasa.ReadSWPort;
import dasa.SWPort;
import dasa.Task;
import dasa.Runnable;
import dasa.TimeEvent;
import dasa.generators.CommonFunctions;
import dasa.generators.IGenerator;

/**
 *
 * @author Stefan Wopperer
 * @author stt32353 Tobias Straubinger
 */
public class TaskGenerator implements IGenerator {

	public static final String taskImplFileName = "tasks.c";
	public static final String taskHeaderFileName = "tasks.h";

	@Override
	public void generate(DASAsystem ds, File root) {
		System.out.println("TaskGenerator started.");
		if (!root.exists())
			root.mkdirs();

		for (Brick b : ds.getBricks()) {

			File bFile = new File(root, b.getName());
			if (!bFile.exists()) {
				bFile.mkdir();
			}
			OSEKSystem os = b.getOs();

			Set<String> usedEvents = new HashSet<>();
			Set<String> usedTasks = new HashSet<>();
			Set<String> usedAlarms = new HashSet<>();
			Set<String> usedRunnables = new HashSet<>();

 			List<String> generatedTaskCodes = new LinkedList<>();

			for (Task t : os.getTasks()) {

				usedTasks.add(t.getName());
				List<String> eventNames = new LinkedList<>();
				List<String> runnableExecutions = new LinkedList<>();

				for (Runnable r : t.getTaskrunnables()) {
					List<String> runEvents = new LinkedList<>();
					for (Event e : r.getEvents()) {
						String rteEventName = null;
						if(e instanceof TimeEvent) {
							TimeEvent te = (TimeEvent) e;
							rteEventName = NamingConventionHelper.getRteTimeEventName(te.getTime());
							usedAlarms.add(NamingConventionHelper.getOilAlarmName(rteEventName, CommonFunctions.getTaskNameFor(e, ds), te.getTime()));
							eventNames.add(rteEventName);
							runEvents.add(rteEventName);
							usedEvents.add(rteEventName);
						} else {
							DataReceivedEvent dr = (DataReceivedEvent) e;
							for(ReadSWPort rsp : dr.getEventports()) {
								rteEventName = NamingConventionHelper.getRteDrEventName(CommonFunctions.getComponentNameFor(rsp, ds), rsp.getName());
								eventNames.add(rteEventName);
								runEvents.add(rteEventName);
								usedEvents.add(rteEventName);
							}
						}
					}
					String runnableEvents = concatStringsWithSep(runEvents, " | ");
					String ifs = TemplateFactory.generateIfEventThenRunnable(
							runnableEvents, r.getName());
					runnableExecutions.add(ifs);
					usedRunnables.add(r.getName());
				}
				String waitEventCode = TemplateFactory
						.generateReadEvent(concatStringsWithSep(eventNames, " | "), t.getName());
				String executeRunnableCode = concatStringsWithSep(runnableExecutions, "\n");

				String taskCode = TemplateFactory.generateTask(t.getName(),
						waitEventCode + executeRunnableCode);
				generatedTaskCodes.add(taskCode);
			}

			try {
				/*
				 * Generate task header
				 */
				File taskHeader = new File(bFile, taskHeaderFileName);
				taskHeader.delete();
				taskHeader.createNewFile();
				System.out.println("Writing task header " + taskHeader.getCanonicalPath());
				RandomAccessFile f = new RandomAccessFile(taskHeader, "rw");
				/* Write wildcard for include information */
				f.writeBytes(TemplateFactory.generateGenericIncludes());
				f.writeBytes("\n");

				/* Write OSEK declarations */
				for (String s : usedTasks) {
					f.writeBytes(SyntaxFactory.generateDeclareTask(s) + "\n");
				}
				if (!usedAlarms.isEmpty())
					f.writeBytes(SyntaxFactory.generateDeclareCounter("Ctr_1_ms") + "\n");

				for (String s : usedAlarms) {
					f.writeBytes(SyntaxFactory.generateDeclareAlarm(s) + "\n");
				}
				for (String s : usedEvents) {
					f.writeBytes(SyntaxFactory.generateDeclareEvent(s) + "\n");
				}

				f.write('\n');
				f.close();

				/*
				 * Generate task implementation
				 */
				File taskFile = new File(bFile, taskImplFileName);
				taskFile.delete();
				taskFile.createNewFile();
				System.out.println("Writing task implementation " + taskFile.getCanonicalPath());
				f = new RandomAccessFile(taskFile, "rw");

				f.writeBytes("#include \"" + taskHeader.getName() + "\"\n\n");

				if (!usedAlarms.isEmpty())
					f.writeBytes(TemplateFactory.generateSetupCounter());

				for(String s : usedRunnables) {
					f.writeBytes("extern void " + s + "();\n");
				}
				f.write('\n');

				/* Write Tasks */
				for (String s : generatedTaskCodes) {
					f.writeBytes(s);
				}
				f.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		}

		System.out.println("TaskGenerator finished.");
	}

	private String concatStringsWithSep(List<String> s, String op) {
		StringBuffer tmp = new StringBuffer();

		for (String st : s) {
			tmp.append(st + op);
		}

		return removeLast(tmp.toString(), op);
	}

	private String removeLast(String text, String regex) {
		return text.substring(0, text.length() - regex.length());
	}

}
