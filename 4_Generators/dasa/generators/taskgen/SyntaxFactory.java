package dasa.generators.taskgen;

public class SyntaxFactory {
	
	public static String generateDeclareTask(String name) {
		return String.format("DeclareTask(%s);", name);
	}
	
	public static String generateDeclareCounter(String name) {
		return String.format("DeclareCounter(%s);", name);
	}
	
	public static String generateDeclareEvent(String name) {
		return String.format("DeclareEvent(%s);", name);
	}
	
	public static String generateDeclareAlarm(String name) {
		return String.format("DeclareAlarm(%s);", name);
	}
	

}
