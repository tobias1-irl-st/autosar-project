package dasa.generators.taskgen;

public class NamingConventionHelper {

	public static String getRteTimeEventName(int time) {
		return String.format("Rte_Ev_TM_Cyclic_%d", time);
	}

	public static String getRteDrEventName(String component, String name) {
		return String.format("Rte_Ev_DR_%s_%s", component, name);
	}

	public static String getOilAlarmName(String eventname, String taskname, int time) {
		return String.format("al_%d_ms_%s_%s", time, eventname, taskname);
	}

}
