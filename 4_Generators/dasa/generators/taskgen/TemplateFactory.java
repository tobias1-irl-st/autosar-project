package dasa.generators.taskgen;

public class TemplateFactory {

	public static String generateTask(String name, String content) {
		String template =
				  "TASK(%s)\n"
				+ "{\n"
				+ "  EventMaskType event;\n"
				+ "  while (1) {\n"
				+ "%s\n"
				+ "  }\n"
				+ "  TerminateTask();\n"
				+ "}\n\n";

		return String.format(template, name, content);
	}


	public static String generateReadEvent(String eventlist, String taskname) {
		String template =
			  "    WaitEvent(%s);\n"
			+ "    GetEvent(%s, &event);\n"
			+ "    ClearEvent(%s);\n\n";
		return String.format(template, eventlist, taskname, eventlist);
	}

	public static String generateIfEventThenRunnable(String eventlist, String runnable) {
		String template =
			  "    if (event & (%s)) {\n"
			+ "      %s();\n"
			+ "    }\n";
		return String.format(template, eventlist, runnable);
	}

	public static String generateSetupCounter() {
		return
			  "void user_1ms_isr_type2(void){\n"
			+ "  StatusType ercd;\n"
			+ "  ercd = SignalCounter(Ctr_1_ms);\n"
			+ "  if( ercd != E_OK ){\n"
			+ "    ShutdownOS( ercd );\n"
			+ "  }\n"
			+ "}\n\n";
	}

	public static String generateGenericIncludes() {
		return
			  "#include \"kernel.h\"\n"
			+ "#include \"kernel_id.h\"\n"
			+ "#include \"ecrobot_interface.h\"\n\n";
	}
}
